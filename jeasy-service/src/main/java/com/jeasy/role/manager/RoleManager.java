package com.jeasy.role.manager;

import com.baomidou.mybatisplus.plugins.Page;
import com.google.common.collect.Lists;
import com.jeasy.base.manager.impl.BaseManagerImpl;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.object.Converter;
import com.jeasy.common.object.MapKit;
import com.jeasy.common.spring.SpringContextHolder;
import com.jeasy.exception.MessageException;
import com.jeasy.role.dao.RoleDAO;
import com.jeasy.role.dto.*;
import com.jeasy.role.entity.RoleEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

/**
 * 角色 Manager
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
@Slf4j
@Component
public class RoleManager extends BaseManagerImpl<RoleDAO, RoleEntity, RoleDTO> {

    /**
     * this is a converter demo only for BeanKit.copyProperties
     *
     * @see BeanKit#copyProperties(Object source, Object target, Converter... converters)
     */
    private static final Converter<String, String> DEMO_CONVERTER = new Converter<String, String>("filed1", "filed2") {
        @Override
        public String convert(final String val) {
            return val;
        }
    };

    public static RoleManager me() {
        return SpringContextHolder.getBean(RoleManager.class);
    }

    public List<RoleListResDTO> list(final RoleListReqDTO roleListReqDTO) {
        RoleDTO roleParamsDTO = new RoleDTO();
        if (!Func.isEmpty(roleListReqDTO)) {
            BeanKit.copyProperties(roleListReqDTO, roleParamsDTO, DEMO_CONVERTER);
        }

        List<RoleDTO> roleDtoList = super.findList(roleParamsDTO);

        if (!Func.isEmpty(roleDtoList)) {
            List<RoleListResDTO> items = Lists.newArrayList();
            for (RoleDTO roleDto : roleDtoList) {
                RoleListResDTO roleListResDTO = new RoleListResDTO();
                BeanKit.copyProperties(roleDto, roleListResDTO, DEMO_CONVERTER);
                items.add(roleListResDTO);
            }
            return items;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<RoleListResDTO> list1_1_0(final RoleListReqDTO roleListReqDTO) {
        return list(roleListReqDTO);
    }

    public List<RoleListResDTO> list1_2_0(final RoleListReqDTO roleListReqDTO) {
        return list(roleListReqDTO);
    }

    public List<RoleListResDTO> list1_3_0(final RoleListReqDTO roleListReqDTO) {
        return list(roleListReqDTO);
    }

    public RoleListResDTO listOne(final RoleListReqDTO roleListReqDTO) {
        RoleDTO roleParamsDTO = new RoleDTO();
        if (!Func.isEmpty(roleListReqDTO)) {
            BeanKit.copyProperties(roleListReqDTO, roleParamsDTO, DEMO_CONVERTER);
        }

        RoleDTO roleDto = super.findOne(roleParamsDTO);
        if (!Func.isEmpty(roleDto)) {
            RoleListResDTO roleListResDTO = new RoleListResDTO();
            BeanKit.copyProperties(roleDto, roleListResDTO, DEMO_CONVERTER);
            return roleListResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Page<RolePageResDTO> pagination(final RolePageReqDTO rolePageReqDTO, final Integer offset, final Integer pageSize) {
        RoleDTO roleParamsDTO = new RoleDTO();
        if (!Func.isEmpty(rolePageReqDTO)) {
            BeanKit.copyProperties(rolePageReqDTO, roleParamsDTO, DEMO_CONVERTER);
        }

        Page<RoleDTO> roleDTOPage = super.findPage(roleParamsDTO, offset, pageSize);

        if (!Func.isEmpty(roleDTOPage)) {
            List<RolePageResDTO> rolePageResDTOs = Lists.newArrayList();
            for (RoleDTO roleDto : roleDTOPage.getRecords()) {
                RolePageResDTO rolePageResDTO = new RolePageResDTO();
                BeanKit.copyProperties(roleDto, rolePageResDTO, DEMO_CONVERTER);
                rolePageResDTOs.add(rolePageResDTO);
            }

            Page<RolePageResDTO> rolePageResDTOPage = new Page<>();
            rolePageResDTOPage.setRecords(rolePageResDTOs);
            rolePageResDTOPage.setTotal(roleDTOPage.getTotal());
            return rolePageResDTOPage;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean add(final RoleAddReqDTO roleAddReqDTO) {
        if (Func.isEmpty(roleAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        RoleDTO roleDto = new RoleDTO();
        BeanKit.copyProperties(roleAddReqDTO, roleDto, DEMO_CONVERTER);
        return super.save(roleDto);
    }

    public Boolean addAllColumn(final RoleAddReqDTO roleAddReqDTO) {
        if (Func.isEmpty(roleAddReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        RoleDTO roleDto = new RoleDTO();
        BeanKit.copyProperties(roleAddReqDTO, roleDto, DEMO_CONVERTER);
        return super.saveAllColumn(roleDto);
    }

    public Boolean addBatchAllColumn(final List<RoleAddReqDTO> roleAddReqDTOList) {
        if (Func.isEmpty(roleAddReqDTOList)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        List<RoleDTO> roleDTOList = Lists.newArrayList();
        for (RoleAddReqDTO roleAddReqDTO : roleAddReqDTOList) {
            RoleDTO roleDto = new RoleDTO();
            BeanKit.copyProperties(roleAddReqDTO, roleDto, DEMO_CONVERTER);
            roleDTOList.add(roleDto);
        }
        return super.saveBatchAllColumn(roleDTOList);
    }

    public RoleShowResDTO show(final Long id) {
        RoleDTO roleDto = super.findById(id);

        if (!Func.isEmpty(roleDto)) {
            RoleShowResDTO roleShowResDTO = new RoleShowResDTO();
            BeanKit.copyProperties(roleDto, roleShowResDTO, DEMO_CONVERTER);
            return roleShowResDTO;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public List<RoleShowResDTO> showByIds(final List<Long> ids) {
        if (Func.isEmpty(ids)) {
            throw new MessageException(ModelResult.CODE_200, "集合不能为空且大小大于0");
        }

        List<RoleDTO> roleDtoList = super.findBatchIds(ids);

        if (!Func.isEmpty(roleDtoList)) {
            List<RoleShowResDTO> roleShowResDTOList = Lists.newArrayList();
            for (RoleDTO roleDto : roleDtoList) {
                RoleShowResDTO roleShowResDTO = new RoleShowResDTO();
                BeanKit.copyProperties(roleDto, roleShowResDTO, DEMO_CONVERTER);
                roleShowResDTOList.add(roleShowResDTO);
            }
            return roleShowResDTOList;
        }
        throw new MessageException(ModelResult.CODE_200, "未查找到记录");
    }

    public Boolean modify(final RoleModifyReqDTO roleModifyReqDTO) {
        if (Func.isEmpty(roleModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }
        RoleDTO roleDto = new RoleDTO();
        BeanKit.copyProperties(roleModifyReqDTO, roleDto, DEMO_CONVERTER);
        return super.modifyById(roleDto);
    }

    public Boolean modifyAllColumn(final RoleModifyReqDTO roleModifyReqDTO) {
        if (Func.isEmpty(roleModifyReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        RoleDTO roleDto = new RoleDTO();
        BeanKit.copyProperties(roleModifyReqDTO, roleDto, DEMO_CONVERTER);
        return super.modifyAllColumnById(roleDto);
    }

    public Boolean removeByParams(final RoleRemoveReqDTO roleRemoveReqDTO) {
        if (Func.isEmpty(roleRemoveReqDTO)) {
            throw new MessageException(ModelResult.CODE_200, "入参不能为空");
        }

        RoleDTO roleParamsDTO = new RoleDTO();
        BeanKit.copyProperties(roleRemoveReqDTO, roleParamsDTO, DEMO_CONVERTER);
        return super.remove(roleParamsDTO);
    }


    @Override
    protected List<RoleDTO> entityToDTOList(final List<RoleEntity> roleEntityList) {
        List<RoleDTO> roleDtoList = Lists.newArrayList();
        if (!Func.isEmpty(roleEntityList)) {
            for (RoleEntity roleEntity : roleEntityList) {
                roleDtoList.add(entityToDTO(roleEntity));
            }
        }
        return roleDtoList;
    }

    @Override
    protected RoleDTO entityToDTO(final RoleEntity roleEntity) {
        RoleDTO roleDto = new RoleDTO();
        if (!Func.isEmpty(roleEntity)) {
            BeanKit.copyProperties(roleEntity, roleDto);
        }
        return roleDto;
    }

    @Override
    protected List<RoleEntity> dtoToEntityList(final List<RoleDTO> roleDtoList) {
        List<RoleEntity> roleEntityList = Lists.newArrayList();
        if (!Func.isEmpty(roleDtoList)) {
            for (RoleDTO roleDto : roleDtoList) {
                roleEntityList.add(dtoToEntity(roleDto));
            }
        }
        return roleEntityList;
    }

    @Override
    protected RoleEntity dtoToEntity(final RoleDTO roleDto) {
        RoleEntity roleEntity = new RoleEntity();
        if (!Func.isEmpty(roleDto)) {
            BeanKit.copyProperties(roleDto, roleEntity);
        }
        return roleEntity;
    }

    @Override
    protected RoleEntity mapToEntity(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new RoleEntity();
        }
        return (RoleEntity) MapKit.toBean(map, RoleEntity.class);
    }

    @Override
    protected RoleDTO mapToDto(final Map<String, Object> map) {
        if (Func.isEmpty(map)) {
            return new RoleDTO();
        }
        return (RoleDTO) MapKit.toBean(map, RoleDTO.class);
    }
}
