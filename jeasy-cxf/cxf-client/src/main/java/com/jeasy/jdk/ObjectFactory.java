
package com.jeasy.jdk;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.jeasy.jdk package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetCurrentUserResponse_QNAME = new QName("http://service.jdk.jeasy.com/", "getCurrentUserResponse");
    private final static QName _SayHiResponse_QNAME = new QName("http://service.jdk.jeasy.com/", "sayHiResponse");
    private final static QName _GetCurrentUser_QNAME = new QName("http://service.jdk.jeasy.com/", "getCurrentUser");
    private final static QName _SayToUser_QNAME = new QName("http://service.jdk.jeasy.com/", "sayToUser");
    private final static QName _SayToUserResponse_QNAME = new QName("http://service.jdk.jeasy.com/", "sayToUserResponse");
    private final static QName _SayHi_QNAME = new QName("http://service.jdk.jeasy.com/", "sayHi");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.jeasy.jdk
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SayToUserResponse }
     * 
     */
    public SayToUserResponse createSayToUserResponse() {
        return new SayToUserResponse();
    }

    /**
     * Create an instance of {@link SayToUser }
     * 
     */
    public SayToUser createSayToUser() {
        return new SayToUser();
    }

    /**
     * Create an instance of {@link SayHiResponse }
     * 
     */
    public SayHiResponse createSayHiResponse() {
        return new SayHiResponse();
    }

    /**
     * Create an instance of {@link GetCurrentUserResponse }
     * 
     */
    public GetCurrentUserResponse createGetCurrentUserResponse() {
        return new GetCurrentUserResponse();
    }

    /**
     * Create an instance of {@link GetCurrentUser }
     * 
     */
    public GetCurrentUser createGetCurrentUser() {
        return new GetCurrentUser();
    }

    /**
     * Create an instance of {@link SayHi }
     * 
     */
    public SayHi createSayHi() {
        return new SayHi();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jdk.jeasy.com/", name = "getCurrentUserResponse")
    public JAXBElement<GetCurrentUserResponse> createGetCurrentUserResponse(GetCurrentUserResponse value) {
        return new JAXBElement<GetCurrentUserResponse>(_GetCurrentUserResponse_QNAME, GetCurrentUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SayHiResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jdk.jeasy.com/", name = "sayHiResponse")
    public JAXBElement<SayHiResponse> createSayHiResponse(SayHiResponse value) {
        return new JAXBElement<SayHiResponse>(_SayHiResponse_QNAME, SayHiResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCurrentUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jdk.jeasy.com/", name = "getCurrentUser")
    public JAXBElement<GetCurrentUser> createGetCurrentUser(GetCurrentUser value) {
        return new JAXBElement<GetCurrentUser>(_GetCurrentUser_QNAME, GetCurrentUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SayToUser }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jdk.jeasy.com/", name = "sayToUser")
    public JAXBElement<SayToUser> createSayToUser(SayToUser value) {
        return new JAXBElement<SayToUser>(_SayToUser_QNAME, SayToUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SayToUserResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jdk.jeasy.com/", name = "sayToUserResponse")
    public JAXBElement<SayToUserResponse> createSayToUserResponse(SayToUserResponse value) {
        return new JAXBElement<SayToUserResponse>(_SayToUserResponse_QNAME, SayToUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SayHi }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://service.jdk.jeasy.com/", name = "sayHi")
    public JAXBElement<SayHi> createSayHi(SayHi value) {
        return new JAXBElement<SayHi>(_SayHi_QNAME, SayHi.class, null, value);
    }

}
