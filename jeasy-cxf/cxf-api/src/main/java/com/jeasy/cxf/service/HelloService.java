package com.jeasy.cxf.service;

import com.jeasy.cxf.entity.User;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface HelloService {

    String sayHi(@WebParam(name = "text") String text);

    String sayToUser(@WebParam(name = "user") User user);

    User getCurrentUser();
}
