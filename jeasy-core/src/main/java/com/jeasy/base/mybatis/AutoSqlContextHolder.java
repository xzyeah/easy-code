package com.jeasy.base.mybatis;

import com.jeasy.common.Func;
import com.jeasy.common.object.BeanKit;
import com.jeasy.common.str.StrKit;
import com.jeasy.exception.KitException;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.mapping.ResultMapping;
import org.apache.ibatis.session.defaults.DefaultSqlSession;
import org.springframework.core.NamedThreadLocal;

import java.util.List;

/**
 * @author TaoBangren
 * @version 1.0
 * @since 2017/5/25 下午10:03
 */
public final class AutoSqlContextHolder {

    private static final String JDBC_TYPE = "jdbcType";

    private AutoSqlContextHolder() {
    }

    private static final NamedThreadLocal<AutoSqlContext> AUTO_SQL_CONTEXT_LOCAL = new NamedThreadLocal<AutoSqlContext>("AUTO_SQL_CONTEXT_LOCAL") {
        protected AutoSqlContext initialValue() {
            return new AutoSqlContext();
        }
    };

    public static final AutoSqlContext buildAutoSqlContext(final String table) {
        AutoSqlContext autoSqlContext = getAutoSqlContext();
        return autoSqlContext;
    }

    public static final AutoSqlContext getAutoSqlContext() {
        return AUTO_SQL_CONTEXT_LOCAL.get();
    }

    public static final String buildInsertSql(final Object parameterObject, final ResultMap resultMap, final boolean isSelective) {
        try {
            if (Func.isEmpty(parameterObject)) {
                throw new KitException("Sorry, I refuse to build sql for a null object!");
            }

            Class parameterClass = parameterObject.getClass();
            Object targetParameter = parameterObject;

            // insertBatchSql
            if (parameterObject instanceof DefaultSqlSession.StrictMap) {
                targetParameter = ((List) ((DefaultSqlSession.StrictMap) parameterObject).get("list")).get(0);
                parameterClass = targetParameter.getClass();
            }

            Entity entity = (Entity) parameterClass.getAnnotation(Entity.class);
            if (Func.isEmpty(entity)) {
                throw new KitException("Sorry, I refuse to build sql for a null @Entity!");
            }

            StringBuilder insertSql = new StringBuilder();
            StringBuilder valueSql = new StringBuilder();

            insertSql.append(DmlOptEnum.INSERT.getName())
                .append(StrKit.C_SPACE).append(StrKit.C_BACK_QUOTE).append(entity.table()).append(StrKit.C_BACK_QUOTE)
                .append(StrKit.C_SPACE).append(StrKit.C_PARENTHESES_START);

            valueSql.append(DmlOptEnum.VALUES.getName())
                .append(StrKit.C_SPACE).append(StrKit.C_PARENTHESES_START);

            List<ResultMapping> propertyResultMappings = resultMap.getPropertyResultMappings();
            for (ResultMapping resultMapping : propertyResultMappings) {

                Object propertyVal = BeanKit.getProperty(targetParameter, resultMapping.getProperty());
                if (isSelective && Func.isEmpty(propertyVal)) {
                    continue;
                }

                insertSql.append(StrKit.C_BACK_QUOTE).append(resultMapping.getColumn()).append(StrKit.C_BACK_QUOTE).append(StrKit.C_COMMA).append(StrKit.C_SPACE);

                valueSql.append(StrKit.C_POUND).append(StrKit.C_DELIM_START)
                    .append(resultMapping.getProperty()).append(StrKit.C_COMMA).append(StrKit.C_SPACE).append(JDBC_TYPE).append(StrKit.C_EQUAL)
                    .append(resultMapping.getJdbcType())
                    .append(StrKit.C_DELIM_END).append(StrKit.C_COMMA).append(StrKit.C_SPACE);
            }

            insertSql.delete(insertSql.lastIndexOf(StrKit.COMMA), insertSql.lastIndexOf(StrKit.COMMA) + 2).append(StrKit.C_PARENTHESES_END);
            valueSql.delete(valueSql.lastIndexOf(StrKit.COMMA), valueSql.lastIndexOf(StrKit.COMMA) + 2).append(StrKit.C_PARENTHESES_END);

            // insertBatchSql
            if (parameterObject instanceof DefaultSqlSession.StrictMap) {
                int size = ((List) ((DefaultSqlSession.StrictMap) parameterObject).get("list")).size();
                String valueUnitSql = valueSql.substring(valueSql.indexOf(DmlOptEnum.VALUES.getName()) + 7);
                for (int i = 1; i < size; i++) {
                    valueSql.append(StrKit.C_COMMA).append(StrKit.C_SPACE).append(valueUnitSql);
                }
            }
            return insertSql.append(StrKit.C_SPACE).append(valueSql).toString();
        } finally {
            AUTO_SQL_CONTEXT_LOCAL.remove();
        }
    }

    public static final String buildSelectByParamsSql(final Object parameterObject, final ResultMap resultMap) {
        try {

        } catch (Exception e) {

        } finally {
            AUTO_SQL_CONTEXT_LOCAL.remove();
        }

        return null;
    }

    public static final String buildCountByParamsSql(final Object parameterObject, final ResultMap resultMap) {
        try {

        } catch (Exception e) {

        } finally {
            AUTO_SQL_CONTEXT_LOCAL.remove();
        }

        return null;
    }

    public static final String buildSelectFirstByParamsSql(final Object parameterObject, final ResultMap resultMap) {
        try {

        } catch (Exception e) {

        } finally {
            AUTO_SQL_CONTEXT_LOCAL.remove();
        }

        return null;
    }

    public static final String buildUpdateByPrimaryKeySelectiveSql(final Object parameterObject, final ResultMap resultMap) {
        try {

        } catch (Exception e) {

        } finally {
            AUTO_SQL_CONTEXT_LOCAL.remove();
        }

        return null;
    }

    public static final String buildUpdateByPrimaryKeySql(final Object parameterObject, final ResultMap resultMap) {
        try {

        } catch (Exception e) {

        } finally {
            AUTO_SQL_CONTEXT_LOCAL.remove();
        }

        return null;
    }

    public static final String buildDeleteByParamsSql(final Object parameterObject, final ResultMap resultMap) {
        try {

        } catch (Exception e) {

        } finally {
            AUTO_SQL_CONTEXT_LOCAL.remove();
        }

        return null;
    }
}
