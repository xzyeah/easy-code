package com.jeasy.base.mybatis;

import com.jeasy.common.str.StrKit;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.builder.SqlSourceBuilder;
import org.apache.ibatis.executor.statement.StatementHandler;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.mapping.ResultMap;
import org.apache.ibatis.mapping.SqlSource;
import org.apache.ibatis.plugin.*;
import org.apache.ibatis.reflection.DefaultReflectorFactory;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.reflection.ReflectorFactory;
import org.apache.ibatis.reflection.factory.DefaultObjectFactory;
import org.apache.ibatis.reflection.factory.ObjectFactory;
import org.apache.ibatis.reflection.wrapper.DefaultObjectWrapperFactory;
import org.apache.ibatis.reflection.wrapper.ObjectWrapperFactory;
import org.apache.ibatis.session.Configuration;

import java.sql.Connection;
import java.util.List;
import java.util.Properties;

@Intercepts({@Signature(type = StatementHandler.class, method = "prepare", args = {Connection.class, Integer.class})})
@Slf4j
public class AutoMapperInterceptor implements Interceptor {

    private static final String INSERT = "insert";

    private static final String INSERT_BATCH = "insertBatch";

    private static final String INSERT_SELECTIVE = "insertSelective";

    private static final String SELECT_BY_PARAMS = "selectByParams";

    private static final String COUNT_BY_PARAMS = "countByParams";

    private static final String SELECT_FIRST_BY_PARAMS = "selectFirstByParams";

    private static final String UPDATE_BY_PRIMARY_KEY_SELECTIVE = "updateByPrimaryKeySelective";

    private static final String UPDATE_BY_PRIMARY_KEY = "updateByPrimaryKey";

    private static final String DELETE_BY_PARAMS = "deleteByParams";

    private static final ObjectFactory DEFAULT_OBJECT_FACTORY = new DefaultObjectFactory();

    private static final ObjectWrapperFactory DEFAULT_OBJECT_WRAPPER_FACTORY = new DefaultObjectWrapperFactory();

    private static final ReflectorFactory DEFAULT_REFLECTOR_FACTORY = new DefaultReflectorFactory();

    @Override
    public Object intercept(final Invocation invocation) throws Throwable {
        StatementHandler statementHandler = (StatementHandler) invocation.getTarget();
        MetaObject metaStatementHandler = MetaObject.forObject(statementHandler, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY, DEFAULT_REFLECTOR_FACTORY);
        // 分离代理对象链
        while (metaStatementHandler.hasGetter("h")) {
            Object object = metaStatementHandler.getValue("h");
            metaStatementHandler = MetaObject.forObject(object, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY, DEFAULT_REFLECTOR_FACTORY);
        }
        // 分离最后一个代理对象的目标类
        while (metaStatementHandler.hasGetter("target")) {
            Object object = metaStatementHandler.getValue("target");
            metaStatementHandler = MetaObject.forObject(object, DEFAULT_OBJECT_FACTORY, DEFAULT_OBJECT_WRAPPER_FACTORY, DEFAULT_REFLECTOR_FACTORY);
        }
        String originalSql = (String) metaStatementHandler.getValue("delegate.boundSql.sql");

        if (StrKit.isBlank(originalSql)) {
            Configuration configuration = (Configuration) metaStatementHandler.getValue("delegate.configuration");
            Object parameterObject = metaStatementHandler.getValue("delegate.boundSql.parameterObject");
            MappedStatement mappedStatement = (MappedStatement) metaStatementHandler.getValue("delegate.mappedStatement");

            String statementId = mappedStatement.getId();
            String statementDao = statementId.substring(0, statementId.lastIndexOf(StrKit.DOT));

            statementId = statementId.substring(statementId.lastIndexOf(StrKit.DOT) + 1);

            String resultMapId = statementDao.substring(statementDao.lastIndexOf(StrKit.DOT) + 1).replace("DAO", "ResultMap");
            ResultMap resultMap = configuration.getResultMap(resultMapId);

            String newSql = "";
            if (INSERT.equals(statementId)) {
                newSql = AutoSqlContextHolder.buildInsertSql(parameterObject, resultMap, false);
            } else if (INSERT_BATCH.equals(statementId)) {
                newSql = AutoSqlContextHolder.buildInsertSql(parameterObject, resultMap, false);
            } else if (INSERT_SELECTIVE.equals(statementId)) {
                newSql = AutoSqlContextHolder.buildInsertSql(parameterObject, resultMap, true);
            } else if (SELECT_BY_PARAMS.equals(statementId)) {
                newSql = AutoSqlContextHolder.buildSelectByParamsSql(parameterObject, resultMap);
            } else if (COUNT_BY_PARAMS.equals(statementId)) {
                newSql = AutoSqlContextHolder.buildCountByParamsSql(parameterObject, resultMap);
            } else if (SELECT_FIRST_BY_PARAMS.equals(statementId)) {
                newSql = AutoSqlContextHolder.buildSelectFirstByParamsSql(parameterObject, resultMap);
            } else if (UPDATE_BY_PRIMARY_KEY_SELECTIVE.equals(statementId)) {
                newSql = AutoSqlContextHolder.buildUpdateByPrimaryKeySelectiveSql(parameterObject, resultMap);
            } else if (UPDATE_BY_PRIMARY_KEY.equals(statementId)) {
                newSql = AutoSqlContextHolder.buildUpdateByPrimaryKeySql(parameterObject, resultMap);
            } else if (DELETE_BY_PARAMS.equals(statementId)) {
                newSql = AutoSqlContextHolder.buildDeleteByParamsSql(parameterObject, resultMap);
            }

            SqlSource sqlSource = buildSqlSource(configuration, newSql, parameterObject.getClass());
            List<ParameterMapping> parameterMappings = sqlSource.getBoundSql(parameterObject).getParameterMappings();
            metaStatementHandler.setValue("delegate.boundSql.sql", sqlSource.getBoundSql(parameterObject).getSql());
            metaStatementHandler.setValue("delegate.boundSql.parameterMappings", parameterMappings);
        }

        // 调用原始statementHandler的prepare方法
        statementHandler = (StatementHandler) metaStatementHandler.getOriginalObject();
        statementHandler.prepare((Connection) invocation.getArgs()[0], (Integer) invocation.getArgs()[1]);
        // 传递给下一个拦截器处理
        return invocation.proceed();
    }

    @Override
    public Object plugin(final Object target) {
        if (target instanceof StatementHandler) {
            return Plugin.wrap(target, this);
        } else {
            return target;
        }
    }

    @Override
    public void setProperties(final Properties properties) {
    }

    private SqlSource buildSqlSource(final Configuration configuration, final String originalSql, final Class<?> parameterType) {
        SqlSourceBuilder builder = new SqlSourceBuilder(configuration);
        return builder.parse(originalSql, parameterType, null);
    }
}
