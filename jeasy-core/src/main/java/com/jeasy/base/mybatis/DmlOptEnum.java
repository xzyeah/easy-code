package com.jeasy.base.mybatis;

/**
 * @author taomk
 * @version 1.0
 * @since 15-6-7 上午6:50
 */
public enum DmlOptEnum {

    AND("AND"),

    OR("OR"),

    IN("IN"),

    NOT_IN("NOT IN"),

    LIKE("LIKE"),

    NOT_LIKE("NOT LIKE"),

    GT(">"),

    LT("<"),

    EQ("="),

    NOT_EQ("!="),

    GT_EQ(">="),

    LT_EQ("<="),

    ASC("ASC"),

    DESC("DESC"),

    LIMIT("LIMIT"),

    WHERE("WHERE"),

    ORDER_BY("ORDER_BY"),

    GROUP_BY("GROUP_BY"),

    INSERT("INSERT INTO"),

    SELECT("SELECT"),

    UPDATE("UPDATE"),

    DELETE("DELETE"),

    VALUES("VALUES"),

    FROM("FROM");

    private String name;

    DmlOptEnum(final String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
