package com.jeasy.base.mybatis;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author TaoBangren
 * @version 1.0
 * @since 2017/5/26 下午6:09
 */
@Data
@AllArgsConstructor
public class DynamicParam {

    private DmlOptEnum opt;

    private String paramField;
}
