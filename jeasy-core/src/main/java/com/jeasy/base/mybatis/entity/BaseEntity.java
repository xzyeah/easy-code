package com.jeasy.base.mybatis.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableLogic;
import com.jeasy.common.thread.ThreadLocalKit;
import lombok.Data;

import java.io.Serializable;

/**
 * BaseEntity
 *
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Data
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 主键
     */
    @TableId
    private Long id;
    /**
     * 是否删除
     */
    @TableLogic
    @TableField("is_del")
    private Integer isDel = 0;
    /**
     * 是否测试
     */
    @TableField("is_test")
    private Integer isTest = ThreadLocalKit.getCurrentUser().getIsTest();
    /**
     * 创建时间
     */
    @TableField("create_at")
    private Long createAt;
    /**
     * 创建者ID
     */
    @TableField("create_by")
    private Long createBy;
    /**
     * 创建者名称
     */
    @TableField("create_name")
    private String createName;
    /**
     * 更新时间
     */
    @TableField("update_at")
    private Long updateAt;
    /**
     * 更新者ID
     */
    @TableField("update_by")
    private Long updateBy;
    /**
     * 更新者名称
     */
    @TableField("update_name")
    private String updateName;

    public final Integer getIsTest() {
        return isTest != null ? isTest : ThreadLocalKit.getCurrentUser().getIsTest();
    }

    public final void setIsTest() {
        this.isTest = ThreadLocalKit.getCurrentUser().getIsTest();
    }
}
