package com.jeasy.base.mybatis;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;
import com.beust.jcommander.internal.Sets;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * @author TaoBangren
 * @version 1.0
 * @since 2017/5/26 下午6:09
 */
@Data
public class AutoSqlContext {

    private final Map<String, List<DynamicParam>> dynamicFields = Maps.newHashMap();

    private final Set<String> dynamicOrFields = Sets.newHashSet();

    private final List<DynamicParam> dynamicOrderFields = Lists.newArrayList();

    private final List<String> insertViewFields = Lists.newArrayList();

    private final List<String> selectViewFields = Lists.newArrayList();

    private final List<String> updateSetFields = Lists.newArrayList();

    public final AutoSqlContext buildInParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.IN, paramField);
    }

    public final AutoSqlContext buildNotInParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.NOT_IN, paramField);
    }

    public final AutoSqlContext buildLikeParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.LIKE, paramField);
    }

    public final AutoSqlContext buildNotLikeParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.NOT_LIKE, paramField);
    }

    public final AutoSqlContext buildEqParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.EQ, paramField);
    }

    public final AutoSqlContext buildNotEqParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.NOT_EQ, paramField);
    }

    public final AutoSqlContext buildLtParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.LT, paramField);
    }

    public final AutoSqlContext buildGtParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.GT, paramField);
    }

    public final AutoSqlContext buildLtEqParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.LT_EQ, paramField);
    }

    public final AutoSqlContext buildGtEqParam(final String entityField, final String paramField) {
        return buildDynamicParam(entityField, DmlOptEnum.GT_EQ, paramField);
    }

    public final AutoSqlContext buildOrderAscParam(final String entityField) {
        dynamicOrderFields.add(new DynamicParam(DmlOptEnum.ASC, entityField));
        return this;
    }

    public final AutoSqlContext buildOrderDescParam(final String entityField) {
        dynamicOrderFields.add(new DynamicParam(DmlOptEnum.DESC, entityField));
        return this;
    }

    public final AutoSqlContext buildOrParam(final String entityField) {
        dynamicOrFields.add(entityField);
        return this;
    }

    public final AutoSqlContext addSelectField(final String entityField) {
        selectViewFields.add(entityField);
        return this;
    }

    public final AutoSqlContext addInsertField(final String entityField) {
        insertViewFields.add(entityField);
        return this;
    }

    public final AutoSqlContext addUpdateField(final String entityField) {
        updateSetFields.add(entityField);
        return this;
    }

    public final AutoSqlContext buildDynamicParam(final String entityField, final DmlOptEnum dmlOpt, final String paramField) {
        List<DynamicParam> dynamicFieldList = Lists.newArrayList();
        if (dynamicFields.containsKey(entityField)) {
            dynamicFieldList = dynamicFields.get(entityField);
        }
        dynamicFieldList.add(new DynamicParam(dmlOpt, paramField));
        dynamicFields.put(entityField, dynamicFieldList);
        return this;
    }
}
