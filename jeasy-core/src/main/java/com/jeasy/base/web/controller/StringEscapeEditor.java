package com.jeasy.base.web.controller;

import com.jeasy.common.web.WafKit;
import org.springframework.web.util.HtmlUtils;

import java.beans.PropertyEditorSupport;

public class StringEscapeEditor extends PropertyEditorSupport {

    public StringEscapeEditor() {
    }

    private final boolean filterXSS = true;

    private final boolean filterSQL = true;

    @Override
    public String getAsText() {
        Object value = getValue();
        return value != null ? value.toString() : "";
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        if (text == null) {
            setValue(null);
        } else {
            if (filterXSS) {
                text = WafKit.stripXSS(text);
            }

            if (filterSQL) {
                text = WafKit.stripSqlInjection(text);
            }
            setValue(HtmlUtils.htmlEscape(text));
        }
    }

}
