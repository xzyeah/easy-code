package com.jeasy.base.web.dto;

import lombok.Data;

import java.util.List;

/**
 * ResultPage
 *
 * @author taomk
 * @version 1.0
 * @since 2015/05/13 17:34
 */
@Data
public final class ResultPage<T> {

    // 当前页码
    private int pageNo = 1;
    // 每页显示数量
    private int pageSize = 10;
    // 总页数
    private int totalPage;
    // 总条数
    private int totalCount;

    // 存放查询结果用的list
    private List<T> items;

    public ResultPage() {
    }

    public ResultPage(int totalCount, int pageSize, int pageNo, List<T> items) {
        this.totalCount = totalCount;
        this.pageSize = pageSize;
        this.pageNo = pageNo;
        this.totalPage = operatorTotalPage();
        this.items = items;
    }

    /**
     * 计算总页数
     *
     * @return
     */
    public int operatorTotalPage() {
        int pageCount = 0;
        if (pageSize != 0) {
            pageCount = totalCount / pageSize;
            if (totalCount % pageSize != 0)
                pageCount++;
        }

        return pageCount;
    }
}
