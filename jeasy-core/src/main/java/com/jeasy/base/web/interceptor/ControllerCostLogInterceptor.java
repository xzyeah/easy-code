package com.jeasy.base.web.interceptor;

import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.common.date.DateKit;
import com.jeasy.common.json.JsonKit;
import com.jeasy.common.thread.ThreadLocalKit;
import com.jeasy.common.web.ResponseKit;
import com.jeasy.common.web.URLKit;
import com.jeasy.doc.util.MonitorUtils;
import com.jeasy.exception.MessageException;
import com.jeasy.exception.ServiceException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Controller层日志拦截器
 *
 * @author taomk
 * @version 1.0
 * @since 15-5-22 下午7:57
 */
@Slf4j
public class ControllerCostLogInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        ThreadLocalKit.putTime(System.currentTimeMillis());
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        long endTime = System.currentTimeMillis();
        long beginTime = ThreadLocalKit.getTime();
        long consumeTime = endTime - beginTime;

        try {
            if (!(handler instanceof HandlerMethod)) {
                return;
            }

            StringBuilder logMsg = new StringBuilder("\nController execute report -------- " + DateKit.currentDatetime() + " -------------------------------------");
            logMsg.append("\nURI         : ").append(request.getRequestURI()).append(", Method : ").append(request.getMethod());
            logMsg.append("\nController  : ").append(((HandlerMethod) handler).getBeanType().getName()).append(", Method : ").append(((HandlerMethod) handler).getMethod().getName());
            logMsg.append("\nDevice Info : ").append(ThreadLocalKit.getDevice() == null ? "" : JsonKit.toJson(ThreadLocalKit.getDevice()));
            logMsg.append("\nUser Info   : ").append(ThreadLocalKit.getCurrentUser() == null ? "" : JsonKit.toJson(ThreadLocalKit.getCurrentUser()));

            if (request.getMethod().equalsIgnoreCase("GET")) {
                logMsg.append("\nQueryString : ").append(URLKit.decodeURLComponent(StringUtils.isBlank(request.getQueryString()) ? "" : request.getQueryString()));
            } else if (request.getMethod().equalsIgnoreCase("POST")) {
                logMsg.append("\nParameter   : ").append(JsonKit.toJson(request.getParameterMap()));
            }

            ModelResult result = (ModelResult) request.getAttribute("result");
            if (ex != null) {

                MonitorUtils.incCountForException(((HandlerMethod) handler).getBeanType().getName(), ((HandlerMethod) handler).getMethod().getName());

                if (result == null) {
                    result = new ModelResult(ModelResult.CODE_500);
                } else {
                    result.setCode(ModelResult.CODE_500);
                }

                if (ex instanceof ServiceException) {
                    result.setCode(((ServiceException) ex).getCode());
                    result.setMessage(ex.getMessage());
                } else if (ex instanceof MessageException) {
                    result.setCode(((MessageException) ex).getCode());
                    result.setMessage(ex.getMessage());
                } else if (ex instanceof UnauthorizedException) {
                    result.setCode(ModelResult.CODE_406);
                    result.setMessage(ModelResult.UNAUTHORIZED);
                } else {
                    result.setMessage(ex.getMessage() == null ? ModelResult.FAIL : ex.getMessage());
                }
            }

            logMsg.append("\nResponse    : ").append(result == null ? StringUtils.EMPTY : JsonKit.toJson(result));
            logMsg.append("\nCost Time   : ").append(consumeTime).append(" ms");
            logMsg.append("\n--------------------------------------------------------------------------------------------");
            log.info(logMsg.toString());

            if (ex != null) {
                log.error(((HandlerMethod) handler).getBeanType().getName() + " Occur Exception : ", ex);
            }

            if ((null == response.getContentType() || !response.getContentType().equalsIgnoreCase("application/octet-stream")) && null != result) {
                ResponseKit.renderJson(response, result);
            }
        } finally {
            ThreadLocalKit.removeDevice();
            ThreadLocalKit.removeTime();
            ThreadLocalKit.removeUser();
            if (handler instanceof HandlerMethod) {
                MonitorUtils.incTimeForController(((HandlerMethod) handler).getBeanType().getName(), ((HandlerMethod) handler).getMethod().getName(), consumeTime);
            }
        }
    }
}
