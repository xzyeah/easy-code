package com.jeasy.base.web.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

/**
 * 自定义Authentication对象，使得Subject除了携带用户的登录名外还可以携带更多信息
 */
@Data
public class CurrentUser implements Serializable {

    private static final long serialVersionUID = -1373760761780840081L;

    private Long id;

    private String loginName;

    private String name;

    private String code;

    private Long roleId;

    private String roleName;

    private String roleCode;

    private String mobile;

    // TODO 定义APP端+PC端当前用户信息 @TaoMingkai
    private Integer isTest = 0;
    private Integer dataSecurityVal = 0;

    private Set<Long> roleIdSet;
    private Set<String> roleNameSet;

    private Set<Long> operationIdSet;
    private Set<String> operationUrlSet;

    /**
     * 本函数输出将作为默认的<shiro:principal/>输出.
     */
    @Override
    public String toString() {
        return loginName;
    }
}
