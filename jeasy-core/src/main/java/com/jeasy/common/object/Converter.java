package com.jeasy.common.object;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author TaoBangren
 * @version 1.0
 * @since 2017/5/8 上午11:35
 */
@Data
@AllArgsConstructor
public abstract class Converter<F, T> {

    public Converter(String property) {
        this.from = property;
        this.to = property;
    }

    public Converter(String from, String to) {
        this.from = from;
        this.to = to;
    }

    private String from;

    private String to;

    private Boolean isLeftSubject = true;

    public abstract T convert(F val);
}
