package com.jeasy.common.cache.ehcache;

/**
 * Ehcache对象序列化接口
 */
public interface IEhcacheSerializer {

    /**
     * 序列化
     *
     * @param value
     * @return
     */
    public String serialize(Object value) throws Exception;

    /**
     * 反序列化对象
     *
     * @param value
     * @return
     */
    public Object deserialize(String value);
}
