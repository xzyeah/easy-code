package com.jeasy.common.json;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.*;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JsonKit {

    public static JSONObject parse(String text) {
        return JSONObject.parseObject(text);
    }

    public static <T> T parse(String text, Class<T> clazz) {
        return JSONObject.parseObject(text, clazz);
    }

    public static <T> T fromJson(String json, Class<T> clazz) {
        Gson gson = new GsonBuilder().disableHtmlEscaping().registerTypeAdapter(
            new TypeToken<Map<String, Object>>() {
            }.getType(),
            new JsonDeserializer<Map<String, Object>>() {
                @Override
                public Map<String, Object> deserialize(
                    JsonElement json, Type typeOfT,
                    JsonDeserializationContext context) throws JsonParseException {
                    Map<String, Object> map = new HashMap<>();
                    JsonObject jsonObject = json.getAsJsonObject();
                    Set<Map.Entry<String, JsonElement>> entrySet = jsonObject.entrySet();
                    for (Map.Entry<String, JsonElement> entry : entrySet) {
                        map.put(entry.getKey(), entry.getValue());
                    }
                    return map;
                }
            }).create();
        return gson.fromJson(json, clazz);
    }

    public static String toJson(Object src) {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        return gson.toJson(src);
    }

    public static String toJsonSerializeNulls(Object src) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.toJson(src);
    }

    public static String get(String key, String snapshot) {
        if (StringUtils.isBlank(key) || StringUtils.isBlank(snapshot)) {
            return null;
        }
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Map<String, String> map = null;
        try {
            map = gson.fromJson(snapshot, new TypeToken<Map<String, String>>() {
            }.getType());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (MapUtils.isEmpty(map)) {
            return null;
        }
        return map.get(key);
    }

    public static String put(String snapshot, String key, String value) {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Map<String, String> map = gson.fromJson(snapshot, new TypeToken<Map<String, String>>() {
        }.getType());
        map.put(key, value);
        return gson.toJson(map);
    }

    public static Map<String, String> jsonToMap(String jsonStr) {
        Gson gson = new GsonBuilder().disableHtmlEscaping().create();
        Type type = new TypeToken<Map<String, String>>() {
        }.getType();
        return gson.fromJson(jsonStr, type);
    }

    public static JSONObject stringToJSONObject(String jsonStr) {
        JSONObject dataJson = JSONObject.parseObject(jsonStr);
        return dataJson;
    }
}

