package com.jeasy.httphelper.request.handler.impl.pro;

import com.jeasy.common.json.JsonKit;
import com.jeasy.httphelper.WSHttpHelperConstant;
import com.jeasy.httphelper.annotation.WSRequest;
import com.jeasy.httphelper.exception.WSException;
import com.jeasy.httphelper.model.ResponseResult;
import com.jeasy.httphelper.model.WSRequestContext;
import com.jeasy.httphelper.request.handler.ResponseProHandler;

public class DefaultResultParseHandlerImpl implements ResponseProHandler {
    @Override
    public ResponseResult handler(WSRequestContext context, ResponseResult result) throws WSException {
        // 解析结果,只解析JSON
        if (context.getResponseType() == WSRequest.ResponseType.JSON) {
            String json = result.getBody().toString();
            Class<?> resultClass = context.getResultClass();
            if (resultClass != null && resultClass != String.class) {
                Object resultObj = JsonKit.fromJson(json, resultClass);
                result.setBody(resultObj);
            }
        }
        return result;
    }

    @Override
    public int level() {
        return WSHttpHelperConstant.PRO_HANDLER_PARSE;
    }
}
