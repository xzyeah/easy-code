package com.jeasy.httphelper.request.handler;

import com.jeasy.httphelper.exception.WSException;
import com.jeasy.httphelper.model.ResponseResult;

public interface CallbackHandler {
    public ResponseResult execute(ResponseResult result) throws WSException;
}
