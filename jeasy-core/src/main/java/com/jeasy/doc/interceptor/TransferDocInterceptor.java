package com.jeasy.doc.interceptor;

import com.beust.jcommander.internal.Lists;
import com.jeasy.base.web.dto.ModelResult;
import com.jeasy.base.web.dto.ResultPage;
import com.jeasy.base.web.resolver.FromJson;
import com.jeasy.common.charset.CharsetKit;
import com.jeasy.common.clazz.ClassKit;
import com.jeasy.common.collection.CollectionKit;
import com.jeasy.common.date.DateKit;
import com.jeasy.common.json.JsonKit;
import com.jeasy.common.str.StrKit;
import com.jeasy.common.thread.ThreadLocalKit;
import com.jeasy.common.web.ResponseKit;
import com.jeasy.doc.annotation.InitField;
import com.jeasy.doc.annotation.MethodDoc;
import com.jeasy.doc.annotation.StatusEnum;
import com.jeasy.doc.model.BodyParam;
import com.jeasy.doc.model.HeaderParam;
import com.jeasy.doc.model.KvParam;
import com.jeasy.doc.model.ResParam;
import com.jeasy.validate.handler.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.net.URLDecoder;
import java.util.*;

@Slf4j
public class TransferDocInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        int isDoc = Integer.valueOf(request.getParameter("doc") == null ? "0" : request.getParameter("doc"));
        int isDisplay = Integer.valueOf(request.getParameter("display") == null ? "0" : request.getParameter("display"));

        StringBuilder logMsg = new StringBuilder("\nAPI doc handle report -------- " + DateKit.currentDatetime() + " -----------------------------------------");
        logMsg.append("\nURI         : ").append(request.getRequestURI()).append(", Method : ").append(request.getMethod());

        if (handler instanceof HandlerMethod) {
            logMsg.append("\nController  : ").append(((HandlerMethod) handler).getBeanType().getName()).append(", Method : ").append(((HandlerMethod) handler).getMethod().getName());
        }

        if (request.getMethod().equalsIgnoreCase("GET")) {
            logMsg.append("\nQueryString : ").append(URLDecoder.decode((request.getQueryString() == null ? "null" : request.getQueryString()), CharsetKit.DEFAULT_ENCODE));
        } else if (request.getMethod().equalsIgnoreCase("POST")) {
            logMsg.append("\nParameter   : ").append(request.getParameterMap());
        }

        // 构建DOC接口响应假数据
        if (isDoc == 1) {
            if (handler instanceof HandlerMethod) {
                Method method = ((HandlerMethod) handler).getMethod();
                MethodDoc methodDoc = method.getAnnotation(MethodDoc.class);
                Class entity = methodDoc.entity();
                Class lists = methodDoc.lists();
                Class pages = methodDoc.pages();

                List<ResParam> resParams = new ArrayList<>();
                long index = 0;
                if (!entity.getSimpleName().equals("Object")) {
                    ResParam codeParam = new ResParam(++index, "code", "200", "HTTP状态", "Integer");
                    resParams.add(codeParam);

                    ResParam dataParam = new ResParam(++index, "data", StringUtils.EMPTY, "响应数据", StringUtils.EMPTY);
                    resParams.add(dataParam);

                    List<ResParam> children = new ArrayList<>();
                    dataParam.setChildren(children);

                    ResParam msgParam = new ResParam(++index, "message", "success", "处理信息", "String");
                    children.add(msgParam);

                    ResParam entityParam = new ResParam(++index, "entity", StringUtils.EMPTY, "实体对象", entity.getSimpleName());
                    children.add(entityParam);

                    List<ResParam> entityChildren = new ArrayList<>();
                    entityParam.setChildren(entityChildren);

                    List<Field> fieldList = getFields(entity);

                    for (Field field : fieldList) {
                        index = buildFieldParamsForDoc(index, entityChildren, field);
                    }
                } else if (!lists.getSimpleName().equals("Object")) {
                    ResParam codeParam = new ResParam(++index, "code", "200", "HTTP状态", "Integer");
                    resParams.add(codeParam);

                    ResParam dataParam = new ResParam(++index, "data", StringUtils.EMPTY, "响应数据", StringUtils.EMPTY);
                    resParams.add(dataParam);

                    List<ResParam> children = new ArrayList<>();
                    dataParam.setChildren(children);

                    ResParam msgParam = new ResParam(++index, "message", "success", "处理信息", "String");
                    children.add(msgParam);

                    ResParam listParam = new ResParam(++index, "list", StringUtils.EMPTY, "集合对象", "List<" + lists.getSimpleName() + ">");
                    children.add(listParam);

                    List<ResParam> listChildren = new ArrayList<>();
                    listParam.setChildren(listChildren);

                    for (int j = 0; j < 3; j++) {
                        ResParam listItem = new ResParam(++index, "[" + j + "]", StringUtils.EMPTY, StringUtils.EMPTY, lists.getSimpleName());
                        listChildren.add(listItem);

                        List<ResParam> itemChildren = new ArrayList<>();
                        listItem.setChildren(itemChildren);

                        List<Field> fieldList = getFields(lists);

                        for (Field field : fieldList) {
                            index = buildFieldParamsForDoc(index, itemChildren, field);
                        }
                    }
                } else if (!pages.getSimpleName().equals("Object")) {
                    ResParam codeParam = new ResParam(++index, "code", "200", "HTTP状态", "Integer");
                    resParams.add(codeParam);

                    ResParam dataParam = new ResParam(++index, "data", StringUtils.EMPTY, "响应数据", StringUtils.EMPTY);
                    resParams.add(dataParam);

                    List<ResParam> children = new ArrayList<>();
                    dataParam.setChildren(children);

                    ResParam msgParam = new ResParam(++index, "message", "success", "处理信息", "String");
                    children.add(msgParam);

                    ResParam recordCountParam = new ResParam(++index, "recordCount", "3", "当前页记录数", "Integer");
                    children.add(recordCountParam);

                    ResParam totalCountParam = new ResParam(++index, "totalCount", "30", "总记录数", "Integer");
                    children.add(totalCountParam);

                    ResParam pageNoParam = new ResParam(++index, "pageNo", "1", "当前页码", "Integer");
                    children.add(pageNoParam);

                    ResParam totalPageParam = new ResParam(++index, "totalPage", "10", "总页数", "Integer");
                    children.add(totalPageParam);

                    ResParam pageSizeParam = new ResParam(++index, "pageSize", "3", "每页大小", "Integer");
                    children.add(pageSizeParam);

                    ResParam listParam = new ResParam(++index, "list", StringUtils.EMPTY, "集合对象", "List<" + pages.getSimpleName() + ">");
                    children.add(listParam);

                    List<ResParam> listChildren = new ArrayList<>();
                    listParam.setChildren(listChildren);

                    for (int j = 0; j < 3; j++) {
                        ResParam listItem = new ResParam(++index, "[" + j + "]", StringUtils.EMPTY, StringUtils.EMPTY, pages.getSimpleName());
                        listChildren.add(listItem);

                        List<ResParam> itemChildren = new ArrayList<>();
                        listItem.setChildren(itemChildren);

                        List<Field> fieldList = getFields(pages);

                        for (Field field : fieldList) {
                            index = buildFieldParamsForDoc(index, itemChildren, field);
                        }
                    }
                } else {
                    ResParam codeParam = new ResParam(++index, "code", "200", "HTTP状态", "Integer");
                    resParams.add(codeParam);

                    ResParam dataParam = new ResParam(++index, "data", StringUtils.EMPTY, "响应数据", StringUtils.EMPTY);
                    resParams.add(dataParam);

                    List<ResParam> children = new ArrayList<>();
                    dataParam.setChildren(children);

                    ResParam msgParam = new ResParam(++index, "message", "success", "处理信息", "String");
                    children.add(msgParam);
                }

                ModelResult modelResult = responseList(ModelResult.CODE_200, ModelResult.SUCCESS, resParams);

                logMsg.append("\nResponse    : ").append(JsonKit.toJson(modelResult));
                logMsg.append("\nCost Time   : ").append(System.currentTimeMillis() - ThreadLocalKit.getTime()).append(" ms");
                logMsg.append("\n--------------------------------------------------------------------------------------------");
                log.info(logMsg.toString());

                if (response.getContentType() == null || !response.getContentType().equalsIgnoreCase("application/octet-stream")) {
                    ResponseKit.renderJson(response, modelResult);
                    return false;
                }
            }
        }

        // 构建右面接口参数
        if (isDisplay == 1) {
            if (handler instanceof HandlerMethod) {

                Method method = ((HandlerMethod) handler).getMethod();
                RequestMapping methodMapping = method.getAnnotation(RequestMapping.class);
                MethodDoc methodDoc = method.getAnnotation(MethodDoc.class);

                Map<String, Object> inputParams = new HashMap<>();

                Map<String, Object> bodyResult = new HashMap<>();
                Map<String, Object> kvResult = new HashMap<>();
                Map<String, Object> headerResult = new HashMap<>();

                List<KvParam> kvParams = new ArrayList<>();
                List<BodyParam> bodyParams = new ArrayList<>();
                List<HeaderParam> headerParams = new ArrayList<>();

                if (methodMapping.headers().length > 0) {
                    HeaderParam version = new HeaderParam("version", methodMapping.headers()[0].split("=")[1], "请求版本", null);
                    headerParams.add(version);
                }

                if (methodMapping.headers().length > 1) {
                    HeaderParam platform = new HeaderParam("platform", methodMapping.headers()[1].split("=")[1], "请求平台", null);
                    headerParams.add(platform);
                }

                if (methodMapping.headers().length > 2) {
                    HeaderParam device = new HeaderParam("device", methodMapping.headers()[2].split("=")[1], "请求设备", null);
                    headerParams.add(device);
                }

                HeaderParam deviceNo = new HeaderParam("deviceNo", "", "设备ID/IP(APP必填,PC可选)", "text");
                headerParams.add(deviceNo);

                HeaderParam deviceModel = new HeaderParam("deviceModel", "", "设备型号(APP必填,PC可选)", "text");
                headerParams.add(deviceModel);

                HeaderParam osVersion = new HeaderParam("osVersion", "", "系统版本(APP必填,PC可选)", "text");
                headerParams.add(osVersion);

                MethodParameter[] methodParameters = ((HandlerMethod) handler).getMethodParameters();
                long index = 4;
                for (MethodParameter methodParameter : methodParameters) {
                    if (methodParameter.hasParameterAnnotation(FromJson.class)) {
                        // 带有@FromJson一律构建为JSON格式参数
                        index = buildJsonParams(bodyParams, index, methodParameter);
                    } else {
                        // 未带有@FromJson一律构建为KV格式参数
                        buildKvParams(kvParams, methodParameter);
                    }
                }

                bodyResult.put("total", bodyParams.size());
                bodyResult.put("rows", bodyParams);

                kvResult.put("total", kvParams.size());
                kvResult.put("rows", kvParams);

                headerResult.put("total", headerParams.size());
                headerResult.put("rows", headerParams);

                inputParams.put("body", bodyResult);
                inputParams.put("kv", kvResult);
                inputParams.put("header", headerResult);

                ModelResult modelResult = responseEntity(ModelResult.CODE_200, ModelResult.SUCCESS, inputParams);

                logMsg.append("\nResponse    : ").append(JsonKit.toJson(modelResult));
                logMsg.append("\nCost Time   : ").append(System.currentTimeMillis() - ThreadLocalKit.getTime()).append(" ms");
                logMsg.append("\n--------------------------------------------------------------------------------------------");
                log.info(logMsg.toString());

                if (response.getContentType() == null || !response.getContentType().equalsIgnoreCase("application/octet-stream")) {
                    ResponseKit.renderJson(response, modelResult);
                    return false;
                }
            }
        }

        if (isDoc == 0 && isDisplay == 0) {
            if (handler instanceof HandlerMethod) {
                Method method = ((HandlerMethod) handler).getMethod();
                MethodDoc methodDoc = method.getAnnotation(MethodDoc.class);
                if (methodDoc == null) {
                    return true;
                }

                StatusEnum statusEnum = methodDoc.status();
                if (statusEnum == StatusEnum.DONE) {
                    return true;
                }

                Class entity = methodDoc.entity();
                Class lists = methodDoc.lists();
                Class pages = methodDoc.pages();

                ModelResult modelResult = new ModelResult();
                if (!entity.getSimpleName().equals("Object")) {
                    modelResult.setCode(ModelResult.CODE_200);
                    modelResult.setMessage(ModelResult.SUCCESS);

                    Object entityObj = ClassKit.newInstance(entity);
                    List<Field> fieldList = getFields(entity);
                    for (Field f : fieldList) {
                        // 跳过private static final变量
                        if (f.getModifiers() != (Modifier.FINAL + Modifier.PRIVATE + Modifier.STATIC)) {
                            f.setAccessible(true);
                            f.set(entityObj, initInstanceForField(f));
                        }
                    }
                    modelResult.setEntity(entityObj);
                } else if (!lists.getSimpleName().equals("Object")) {
                    modelResult.setCode(ModelResult.CODE_200);
                    modelResult.setMessage(ModelResult.SUCCESS);

                    List<Object> entityList = Lists.newArrayList();
                    for (int j = 0; j < 3; j++) {
                        Object entityObj = ClassKit.newInstance(lists);
                        List<Field> fieldList = getFields(lists);
                        for (Field f : fieldList) {
                            // 跳过private static final变量
                            if (f.getModifiers() != (Modifier.FINAL + Modifier.PRIVATE + Modifier.STATIC)) {
                                f.setAccessible(true);
                                f.set(entityObj, initInstanceForField(f));
                            }
                        }
                        entityList.add(entityObj);
                    }
                    modelResult.setList(entityList);
                } else if (!pages.getSimpleName().equals("Object")) {
                    modelResult.setCode(ModelResult.CODE_200);
                    modelResult.setMessage(ModelResult.SUCCESS);

                    ResultPage resultPage = new ResultPage();
                    resultPage.setPageNo(1);
                    resultPage.setTotalCount(30);
                    resultPage.setPageSize(3);
                    resultPage.setTotalPage(10);

                    List<Object> entityList = Lists.newArrayList();
                    for (int j = 0; j < 3; j++) {
                        Object entityObj = ClassKit.newInstance(pages);
                        List<Field> fieldList = getFields(pages);
                        for (Field f : fieldList) {
                            // 跳过private static final变量
                            if (f.getModifiers() != (Modifier.FINAL + Modifier.PRIVATE + Modifier.STATIC)) {
                                f.setAccessible(true);
                                f.set(entityObj, initInstanceForField(f));
                            }
                        }
                        entityList.add(entityObj);
                    }
                    resultPage.setItems(entityList);
                    modelResult.setResultPage(resultPage);
                } else {
                    modelResult.setCode(ModelResult.CODE_200);
                    modelResult.setMessage(ModelResult.SUCCESS);
                }

                logMsg.append("\nResponse    : ").append(JsonKit.toJson(modelResult));
                logMsg.append("\nCost Time   : ").append(System.currentTimeMillis() - ThreadLocalKit.getTime()).append(" ms");
                logMsg.append("\n--------------------------------------------------------------------------------------------");
                log.info(logMsg.toString());

                if (response.getContentType() == null || !response.getContentType().equalsIgnoreCase("application/octet-stream")) {
                    ResponseKit.renderJson(response, modelResult);
                    return false;
                }
            }
        }

        return true;
    }

    private Object initInstanceForField(Field field) throws IllegalAccessException {

        InitField initField = field.getAnnotation(InitField.class);

        if (initField == null) {
            return null;
        }

        if (List.class.isAssignableFrom(field.getType())) {
            Class genericClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
            List fieldValue = JsonKit.fromJson(initField.value(), List.class);

            if (String.class.isAssignableFrom(genericClass)) {
                List<String> initList = Lists.newArrayList();
                for (Object obj : fieldValue) {
                    initList.add(String.valueOf(obj.toString()));
                }
                return initList;
            } else if (Long.class.isAssignableFrom(genericClass)) {
                List<Long> initList = Lists.newArrayList();
                for (Object obj : fieldValue) {
                    initList.add(Long.valueOf(obj.toString()));
                }
                return initList;
            } else if (Integer.class.isAssignableFrom(genericClass)) {
                List<Integer> initList = Lists.newArrayList();
                for (Object obj : fieldValue) {
                    initList.add(Integer.valueOf(obj.toString()));
                }
                return initList;
            } else {
                List<Object> initList = Lists.newArrayList();
                List<Field> fieldList = getFields(genericClass);
                for (int i = 0; i < 3; i++) {
                    Object entityObj = ClassKit.newInstance(genericClass);
                    for (Field f : fieldList) {
                        // 跳过private static final变量
                        if (f.getModifiers() != (Modifier.FINAL + Modifier.PRIVATE + Modifier.STATIC)) {
                            f.setAccessible(true);
                            f.set(entityObj, initInstanceForField(f));
                        }
                    }
                    initList.add(entityObj);
                }
                return initList;
            }
        } else if (field.getType().isArray()) {
            Class genericClass = field.getType().getComponentType();
            List fieldValue = JsonKit.fromJson(initField.value(), List.class);

            if (String.class.isAssignableFrom(genericClass)) {
                List<String> initList = Lists.newArrayList();
                for (Object obj : fieldValue) {
                    initList.add(String.valueOf(obj.toString()));
                }
                return initList.toArray(new String[initList.size()]);
            } else if (Long.class.isAssignableFrom(genericClass)) {
                List<Long> initList = Lists.newArrayList();
                for (Object obj : fieldValue) {
                    initList.add(Long.valueOf(obj.toString()));
                }
                return initList.toArray(new Long[initList.size()]);
            } else if (Integer.class.isAssignableFrom(genericClass)) {
                List<Integer> initList = Lists.newArrayList();
                for (Object obj : fieldValue) {
                    initList.add(Integer.valueOf(obj.toString()));
                }
                return initList.toArray(new Integer[initList.size()]);
            } else {
                List<Object> initList = Lists.newArrayList();
                List<Field> fieldList = getFields(genericClass);
                for (int i = 0; i < 3; i++) {
                    Object entityObj = ClassKit.newInstance(genericClass);
                    for (Field f : fieldList) {
                        // 跳过private static final变量
                        if (f.getModifiers() != (Modifier.FINAL + Modifier.PRIVATE + Modifier.STATIC)) {
                            f.setAccessible(true);
                            f.set(entityObj, initInstanceForField(f));
                        }
                    }
                    initList.add(entityObj);
                }
                return initList.toArray(new Object[initList.size()]);
            }
        } else if (Set.class.isAssignableFrom(field.getType())) {
            Class genericClass = field.getType().getComponentType();
            List fieldValue = JsonKit.fromJson(initField.value(), List.class);

            if (String.class.isAssignableFrom(genericClass)) {
                List<String> initList = Lists.newArrayList();
                for (Object obj : fieldValue) {
                    initList.add(String.valueOf(obj.toString()));
                }
                return CollectionKit.toHashSet(initList);
            } else if (Long.class.isAssignableFrom(genericClass)) {
                List<Long> initList = Lists.newArrayList();
                for (Object obj : fieldValue) {
                    initList.add(Long.valueOf(obj.toString()));
                }
                return CollectionKit.toHashSet(initList);
            } else if (Integer.class.isAssignableFrom(genericClass)) {
                List<Integer> initList = Lists.newArrayList();
                for (Object obj : fieldValue) {
                    initList.add(Integer.valueOf(obj.toString()));
                }
                return CollectionKit.toHashSet(initList);
            } else {
                List<Object> initList = Lists.newArrayList();
                List<Field> fieldList = getFields(genericClass);
                for (int i = 0; i < 3; i++) {
                    Object entityObj = ClassKit.newInstance(genericClass);
                    for (Field f : fieldList) {
                        // 跳过private static final变量
                        if (f.getModifiers() != (Modifier.FINAL + Modifier.PRIVATE + Modifier.STATIC)) {
                            f.setAccessible(true);
                            f.set(entityObj, initInstanceForField(f));
                        }
                    }
                    initList.add(entityObj);
                }
                return CollectionKit.toHashSet(initList);
            }
        } else if (String.class.isAssignableFrom(field.getType())) {
            return StrKit.isEmpty(initField.value()) ? "" : String.valueOf(initField.value());
        } else if (Long.class.isAssignableFrom(field.getType())) {
            return StrKit.isEmpty(initField.value()) ? 0 : Long.valueOf(initField.value());
        } else if (Integer.class.isAssignableFrom(field.getType())) {
            return StrKit.isEmpty(initField.value()) ? 0 : Integer.valueOf(initField.value());
        } else {
            List<Field> fieldList = getFields(field.getType());
            Object entityObj = ClassKit.newInstance(field.getType());
            for (Field f : fieldList) {
                // 跳过private static final变量
                if (f.getModifiers() != (Modifier.FINAL + Modifier.PRIVATE + Modifier.STATIC)) {
                    f.setAccessible(true);
                    f.set(entityObj, initInstanceForField(f));
                }
            }
            return entityObj;
        }
    }

    private long buildFieldParamsForDoc(long index, List<ResParam> itemChildren, Field field) {
        InitField initField = field.getAnnotation(InitField.class);
        if (initField == null) {
            return index;
        }

        ResParam fieldParam = new ResParam(++index, field.getName(), StringUtils.EMPTY, initField.desc(), field.getType().getSimpleName());
        if (field.getType().equals(List.class) || field.getType().equals(Set.class)) {
            Class genericClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
            index = buildCollectionParamsForDoc(index, field, initField, fieldParam, genericClass);
        } else if (field.getType().isArray()) {
            Class genericClass = field.getType().getComponentType();
            index = buildCollectionParamsForDoc(index, field, initField, fieldParam, genericClass);
        } else if (StringUtils.isBlank(initField.value())) {
            List<ResParam> fieldParams = new ArrayList<>();
            fieldParam.setChildren(fieldParams);

            List<Field> fieldList = getFields(field.getType());

            for (Field f : fieldList) {
                index = buildFieldParamsForDoc(index, fieldParams, f);
            }
        } else {
            fieldParam.setValue(initField.value());
        }

        itemChildren.add(fieldParam);
        return index;
    }

    private long buildCollectionParamsForDoc(long index, Field field, InitField initField, ResParam fieldParam, Class genericClass) {
        List fieldValue = JsonKit.fromJson(initField.value(), List.class);

        List<ResParam> fieldChildren = new ArrayList<>();
        fieldParam.setChildren(fieldChildren);

        if (fieldValue != null) {
            int i = 0;
            for (Object item : fieldValue) {
                ResParam itemParam = new ResParam(++index, "[" + i++ + "]", item.toString(), StringUtils.EMPTY, genericClass.getSimpleName());
                fieldChildren.add(itemParam);
            }
        } else {
            if (field.getType().isArray()) {
                genericClass = field.getType().getComponentType();
            } else {
                genericClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
            }

            List<Field> fieldList = getFields(genericClass);

            for (int i = 0; i < 3; i++) {
                ResParam itemParam = new ResParam(++index, "[" + i + "]", StringUtils.EMPTY, StringUtils.EMPTY, genericClass.getSimpleName());
                fieldChildren.add(itemParam);

                List<ResParam> fieldParams = new ArrayList<>();
                itemParam.setChildren(fieldParams);
                for (Field f : fieldList) {
                    index = buildFieldParamsForDoc(index, fieldParams, f);
                }
            }
        }
        return index;
    }

    private void buildKvParams(List<KvParam> kvParams, MethodParameter methodParameter) {
        InitField initField = methodParameter.getParameterAnnotation(InitField.class);
        if (initField == null) {
            return;
        }

        if (methodParameter.getParameterType().equals(String.class)) {
            KvParam kvParam = new KvParam(initField.name(), initField.value(), "text", "String", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
            kvParams.add(kvParam);
        } else if (methodParameter.getParameterType().equals(Long.class)) {
            KvParam kvParam = new KvParam(initField.name(), initField.value(), "text", "Long", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
            kvParams.add(kvParam);
        } else if (methodParameter.getParameterType().equals(Integer.class)) {
            KvParam kvParam = new KvParam(initField.name(), initField.value(), "text", "Integer", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
            kvParams.add(kvParam);
        } else if (methodParameter.getParameterType().equals(Double.class)) {
            KvParam kvParam = new KvParam(initField.name(), initField.value(), "text", "Double", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
            kvParams.add(kvParam);
        } else if (methodParameter.getParameterType().equals(Float.class)) {
            KvParam kvParam = new KvParam(initField.name(), initField.value(), "text", "Float", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
            kvParams.add(kvParam);
        } else if (methodParameter.getParameterType().equals(List.class)) {
            Class genericClass = (Class) ((ParameterizedType) methodParameter.getGenericParameterType()).getActualTypeArguments()[0];
            List fieldValue = JsonKit.fromJson(initField.value(), List.class);
            if (fieldValue != null) {
                buildCollectionParams(kvParams, methodParameter, initField, genericClass, fieldValue);
            }
        } else if (methodParameter.getParameterType().equals(Set.class)) {
            Class genericClass = (Class) ((ParameterizedType) methodParameter.getGenericParameterType()).getActualTypeArguments()[0];
            List fieldValue = JsonKit.fromJson(initField.value(), List.class);
            if (fieldValue != null) {
                buildCollectionParams(kvParams, methodParameter, initField, genericClass, fieldValue);
            }
        } else if (methodParameter.getParameterType().isArray()) {
            Class genericClass = methodParameter.getParameterType().getComponentType();
            List fieldValue = JsonKit.fromJson(initField.value(), List.class);
            if (fieldValue != null) {
                buildCollectionParams(kvParams, methodParameter, initField, genericClass, fieldValue);
            }
        } else {
            Class genericClass = methodParameter.getParameterType();
            if (StringUtils.isBlank(initField.value())) {
                buildObjectKvParams(kvParams, genericClass);
            }
        }
    }

    private void buildObjectKvParams(List<KvParam> kvParams, Class genericClass) {
        List<Field> fieldList = getFields(genericClass);
        for (Field field : fieldList) {
            InitField initField = field.getAnnotation(InitField.class);
            if (field.getType().equals(String.class)) {
                KvParam kvParam = new KvParam(field.getName(), initField.value(), "text", "String", initField.desc(), buildRuleDesc(field.getAnnotations()));
                kvParams.add(kvParam);
            } else if (field.getType().equals(Long.class)) {
                KvParam kvParam = new KvParam(field.getName(), Long.valueOf(initField.value()).toString(), "text", "Long", initField.desc(), buildRuleDesc(field.getAnnotations()));
                kvParams.add(kvParam);
            } else if (field.getType().equals(Integer.class)) {
                KvParam kvParam = new KvParam(field.getName(), Integer.valueOf(initField.value()).toString(), "text", "Integer", initField.desc(), buildRuleDesc(field.getAnnotations()));
                kvParams.add(kvParam);
            } else if (field.getType().equals(Double.class)) {
                KvParam kvParam = new KvParam(field.getName(), Double.valueOf(initField.value()).toString(), "text", "Double", initField.desc(), buildRuleDesc(field.getAnnotations()));
                kvParams.add(kvParam);
            } else if (field.getType().equals(Float.class)) {
                KvParam kvParam = new KvParam(field.getName(), Float.valueOf(initField.value()).toString(), "text", "Float", initField.desc(), buildRuleDesc(field.getAnnotations()));
                kvParams.add(kvParam);
            }
        }
    }

    private void buildCollectionParams(List<KvParam> kvParams, MethodParameter methodParameter, InitField initField, Class genericClass, List fieldValue) {
        int i = 0;
        for (Object item : fieldValue) {
            if (genericClass.equals(String.class)) {
                KvParam kvParam = new KvParam(initField.name() + "[" + i++ + "]", item.toString(), "text", "String", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
                kvParams.add(kvParam);
            } else if (genericClass.equals(Long.class)) {
                KvParam kvParam = new KvParam(initField.name() + "[" + i++ + "]", Long.valueOf(item.toString()).toString(), "text", "Long", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
                kvParams.add(kvParam);
            } else if (genericClass.equals(Integer.class)) {
                KvParam kvParam = new KvParam(initField.name() + "[" + i++ + "]", Integer.valueOf(item.toString()).toString(), "text", "Integer", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
                kvParams.add(kvParam);
            } else if (genericClass.equals(Double.class)) {
                KvParam kvParam = new KvParam(initField.name() + "[" + i++ + "]", Double.valueOf(item.toString()).toString(), "text", "Double", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
                kvParams.add(kvParam);
            } else if (genericClass.equals(Float.class)) {
                KvParam kvParam = new KvParam(initField.name() + "[" + i++ + "]", Float.valueOf(item.toString()).toString(), "text", "Float", initField.desc(), buildRuleDesc(methodParameter.getParameterAnnotations()));
                kvParams.add(kvParam);
            }
        }
    }

    private long buildJsonParams(List<BodyParam> bodyParams, long index, MethodParameter methodParameter) {
        Class curClass = methodParameter.getParameterType();
        if (curClass.equals(String.class)
            || curClass.equals(Long.class)
            || curClass.equals(Integer.class)
            || curClass.equals(Double.class)
            || curClass.equals(Float.class)) {
            // 构建基本类型参数
            InitField initField = methodParameter.getParameterAnnotation(InitField.class);
            if (initField == null) {
                return index;
            }

            BodyParam fieldParam = new BodyParam(++index, initField.name(), initField.value(), initField.desc(), curClass.getSimpleName(), buildRuleDesc(methodParameter.getParameterAnnotations()));
            bodyParams.add(fieldParam);
        } else if (curClass.equals(List.class) || curClass.equals(Set.class) || curClass.isArray()) {
            // 构建集合类型参数
            InitField initField = methodParameter.getParameterAnnotation(InitField.class);
            if (initField == null) {
                return index;
            }
            BodyParam fieldParam = new BodyParam(++index, initField.name(), StringUtils.EMPTY, initField.desc(), curClass.getSimpleName(), buildRuleDesc(methodParameter.getParameterAnnotations()));
            bodyParams.add(fieldParam);

            List fieldValue = JsonKit.fromJson(initField.value(), List.class);
            if (fieldValue != null) {
                // 构建基本包装类型集合参数
                if (curClass.isArray()) {
                    Class genericClass = methodParameter.getParameterType().getComponentType();
                    index = buildFieldParams(bodyParams, ++index, fieldParam, fieldValue, genericClass);
                } else {
                    Class genericClass = (Class) ((ParameterizedType) methodParameter.getGenericParameterType()).getActualTypeArguments()[0];
                    index = buildFieldParams(bodyParams, ++index, fieldParam, fieldValue, genericClass);
                }
            } else {
                // 构建Object类型集合参数
                if (curClass.isArray()) {
                    int recursiveCount = 0;
                    Class genericClass = methodParameter.getParameterType().getComponentType();
                    index = buildObjectsParams(++recursiveCount, bodyParams, ++index, fieldParam, genericClass);
                } else {
                    int recursiveCount = 0;
                    Class genericClass = (Class) ((ParameterizedType) methodParameter.getGenericParameterType()).getActualTypeArguments()[0];
                    index = buildObjectsParams(++recursiveCount, bodyParams, ++index, fieldParam, genericClass);
                }
            }
        } else {
            // 构建Object类型参数
            List<Field> fieldList = getFields(curClass);
            for (Field field : fieldList) {
                InitField initField = field.getAnnotation(InitField.class);
                if (initField != null) {
                    BodyParam fieldParam = new BodyParam(++index, field.getName(), StringUtils.EMPTY, initField.desc(), field.getType().getSimpleName(), buildRuleDesc(field.getAnnotations()));
                    bodyParams.add(fieldParam);
                    if (field.getType().equals(List.class) || field.getType().isArray()) {
                        if (StringUtils.isNotBlank(initField.value())) {
                            List fieldValue = JsonKit.fromJson(initField.value(), List.class);
                            if (fieldValue != null) {
                                // 构建基本包装类型集合参数
                                if (field.getType().isArray()) {
                                    Class genericClass = field.getType().getComponentType();
                                    index = buildFieldParams(bodyParams, ++index, fieldParam, fieldValue, genericClass);
                                } else {
                                    Class genericClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                                    index = buildFieldParams(bodyParams, ++index, fieldParam, fieldValue, genericClass);
                                }
                            }
                        } else {
                            // 构建Object类型集合参数
                            if (field.getType().isArray()) {
                                int recursiveCount = 0;
                                Class genericClass = field.getType().getComponentType();
                                index = buildObjectsParams(++recursiveCount, bodyParams, ++index, fieldParam, genericClass);
                            } else {
                                int recursiveCount = 0;
                                Class genericClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                                index = buildObjectsParams(++recursiveCount, bodyParams, ++index, fieldParam, genericClass);
                            }
                        }
                    } else if (field.getType().equals(String.class)
                        || field.getType().equals(Long.class)
                        || field.getType().equals(Integer.class)
                        || field.getType().equals(Double.class)
                        || field.getType().equals(Float.class)) {
                        fieldParam.setValue(initField.value());
                        fieldParam.setEditor("text");
                    } else {
                        // 对象类型
                        if (StringUtils.isBlank(initField.value())) {
                            int recursiveCount = 0;
                            index = buildObjectParams(++recursiveCount, bodyParams, ++index, fieldParam, field.getType());
                        }
                    }
                }
            }
        }
        return index;
    }

    private long buildObjectsParams(int recursiveCount, List<BodyParam> bodyParams, long index, BodyParam fieldParam, Class clazz) {
        List<Field> fieldList = getFields(clazz);
        for (int i = 0; i < 3; i++) {
            BodyParam param = new BodyParam(++index, "[" + i + "]", StringUtils.EMPTY, StringUtils.EMPTY, clazz.getSimpleName(), StringUtils.EMPTY);
            param.set_parentId(fieldParam.getId());
            bodyParams.add(param);

            for (Field field : fieldList) {
                // 基本包装类型
                if (field.getType().equals(String.class)
                    || field.getType().equals(Long.class)
                    || field.getType().equals(Integer.class)
                    || field.getType().equals(Double.class)
                    || field.getType().equals(Float.class)) {
                    InitField initField = field.getAnnotation(InitField.class);
                    if (initField != null) {
                        BodyParam itemParam = new BodyParam(++index, field.getName(), initField.value(), StringUtils.EMPTY, field.getType().getSimpleName(), buildRuleDesc(field.getAnnotations()));
                        itemParam.setDesc(initField.desc());
                        itemParam.set_parentId(param.getId());
                        itemParam.setEditor("text");
                        bodyParams.add(itemParam);
                    }
                } else if (field.getType().equals(List.class) || field.getType().isArray()) {
                    // 集合类型 仅支持List/Array
                    Class genericClass;
                    if (field.getType().isArray()) {
                        genericClass = field.getType().getComponentType();
                    } else {
                        genericClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                    }

                    InitField initField = field.getAnnotation(InitField.class);
                    if (initField != null) {
                        BodyParam itemParam = new BodyParam(++index, field.getName(), StringUtils.EMPTY, StringUtils.EMPTY, field.getType().getSimpleName(), buildRuleDesc(field.getAnnotations()));
                        itemParam.setDesc(initField.desc());
                        itemParam.set_parentId(param.getId());
                        itemParam.setEditor("text");
                        bodyParams.add(itemParam);
                        // 构建：集合元素为基本类型
                        if (StringUtils.isNotBlank(initField.value())) {
                            List fieldValue = JsonKit.fromJson(initField.value(), List.class);
                            index = buildFieldParams(bodyParams, ++index, itemParam, fieldValue, genericClass);
                        } else {
                            // 构建：集合元素为Object类型
                            index = buildObjectsParams(++recursiveCount, bodyParams, ++index, itemParam, genericClass);
                        }
                    }
                } else {
                    // 对象类型
                    InitField initField = field.getAnnotation(InitField.class);
                    if (initField != null) {
                        BodyParam itemParam = new BodyParam(++index, field.getName(), StringUtils.EMPTY, StringUtils.EMPTY, field.getType().getSimpleName(), buildRuleDesc(field.getAnnotations()));
                        itemParam.setDesc(initField.desc());
                        itemParam.set_parentId(param.getId());
                        itemParam.setEditor("text");
                        bodyParams.add(itemParam);

                        if (StringUtils.isBlank(initField.value())) {
                            index = buildObjectParams(++recursiveCount, bodyParams, ++index, itemParam, field.getType());
                        }
                    }
                }
            }
        }
        return index;
    }

    private long buildObjectParams(int recursiveCount, List<BodyParam> bodyParams, long index, BodyParam fieldParam, Class clazz) {
        List<Field> fieldList = getFields(clazz);
        for (Field field : fieldList) {
            // 基本包装类型
            if (field.getType().equals(String.class)
                || field.getType().equals(Long.class)
                || field.getType().equals(Integer.class)
                || field.getType().equals(Double.class)
                || field.getType().equals(Float.class)) {
                InitField initField = field.getAnnotation(InitField.class);
                if (initField != null) {
                    BodyParam itemParam = new BodyParam(++index, field.getName(), initField.value(), StringUtils.EMPTY, field.getType().getSimpleName(), buildRuleDesc(field.getAnnotations()));
                    itemParam.setDesc(initField.desc());
                    itemParam.set_parentId(fieldParam.getId());
                    itemParam.setEditor("text");
                    bodyParams.add(itemParam);
                }
            } else if (field.getType().equals(List.class) || field.getType().isArray()) {
                // 集合类型 仅支持List/Array
                Class genericClass;
                if (field.getType().isArray()) {
                    genericClass = field.getType().getComponentType();
                } else {
                    genericClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                }

                InitField initField = field.getAnnotation(InitField.class);
                if (initField != null) {
                    BodyParam itemParam = new BodyParam(++index, field.getName(), StringUtils.EMPTY, StringUtils.EMPTY, field.getType().getSimpleName(), buildRuleDesc(field.getAnnotations()));
                    itemParam.setDesc(initField.desc());
                    itemParam.set_parentId(fieldParam.getId());
                    itemParam.setEditor("text");
                    bodyParams.add(itemParam);
                    // 构建：集合元素为基本类型
                    if (StringUtils.isNotBlank(initField.value())) {
                        List fieldValue = JsonKit.fromJson(initField.value(), List.class);
                        index = buildFieldParams(bodyParams, ++index, itemParam, fieldValue, genericClass);
                    } else {
                        // 构建：集合元素为Object类型
                        index = buildObjectsParams(++recursiveCount, bodyParams, ++index, itemParam, genericClass);
                    }
                }
            } else {
                // 对象类型
                InitField initField = field.getAnnotation(InitField.class);
                if (initField != null) {
                    BodyParam itemParam = new BodyParam(++index, field.getName(), StringUtils.EMPTY, StringUtils.EMPTY, field.getType().getSimpleName(), buildRuleDesc(field.getAnnotations()));
                    itemParam.setDesc(initField.desc());
                    itemParam.set_parentId(fieldParam.getId());
                    itemParam.setEditor("text");
                    bodyParams.add(itemParam);

                    if (StringUtils.isBlank(initField.value())) {
                        index = buildObjectParams(++recursiveCount, bodyParams, ++index, itemParam, field.getType());
                    }
                }
            }
        }
        return index;
    }

    private long buildFieldParams(List<BodyParam> bodyParams, long index, BodyParam fieldParam, List fieldValue, Class genericClass) {
        int i = 0;
        for (Object item : fieldValue) {
            if (genericClass.equals(String.class)) {
                BodyParam itemParam = new BodyParam(++index, "[" + i++ + "]", item.toString(), StringUtils.EMPTY, genericClass.getSimpleName(), StringUtils.EMPTY);
                itemParam.set_parentId(fieldParam.getId());
                itemParam.setEditor("text");
                bodyParams.add(itemParam);
            } else if (genericClass.equals(Long.class)) {
                BodyParam itemParam = new BodyParam(++index, "[" + i++ + "]", Long.valueOf(item.toString()).toString(), StringUtils.EMPTY, genericClass.getSimpleName(), StringUtils.EMPTY);
                itemParam.set_parentId(fieldParam.getId());
                itemParam.setEditor("text");
                bodyParams.add(itemParam);
            } else if (genericClass.equals(Integer.class)) {
                BodyParam itemParam = new BodyParam(++index, "[" + i++ + "]", Integer.valueOf(item.toString()).toString(), StringUtils.EMPTY, genericClass.getSimpleName(), StringUtils.EMPTY);
                itemParam.set_parentId(fieldParam.getId());
                itemParam.setEditor("text");
                bodyParams.add(itemParam);
            } else if (genericClass.equals(Double.class)) {
                BodyParam itemParam = new BodyParam(++index, "[" + i++ + "]", Double.valueOf(item.toString()).toString(), StringUtils.EMPTY, genericClass.getSimpleName(), StringUtils.EMPTY);
                itemParam.set_parentId(fieldParam.getId());
                itemParam.setEditor("text");
                bodyParams.add(itemParam);
            } else if (genericClass.equals(Float.class)) {
                BodyParam itemParam = new BodyParam(++index, "[" + i++ + "]", Float.valueOf(item.toString()).toString(), StringUtils.EMPTY, genericClass.getSimpleName(), StringUtils.EMPTY);
                itemParam.set_parentId(fieldParam.getId());
                itemParam.setEditor("text");
                bodyParams.add(itemParam);
            }
        }
        return index;
    }

    private String buildRuleDesc(Annotation[] annotations) {
        StringBuilder rule = new StringBuilder();
        for (Annotation annotation : annotations) {
            if (ValidateDigit.class.isInstance(annotation)) {
                rule.append(((ValidateDigit) annotation).message()).append(";");
            } else if (ValidateInt.class.isInstance(annotation)) {
                rule.append(((ValidateInt) annotation).message()).append(";");
            } else if (ValidateLength.class.isInstance(annotation)) {
                rule.append(((ValidateLength) annotation).message()).append(";");
            } else if (ValidateLong.class.isInstance(annotation)) {
                rule.append(((ValidateLong) annotation).message()).append(";");
            } else if (ValidateNotEmpty.class.isInstance(annotation)) {
                rule.append(((ValidateNotEmpty) annotation).message()).append(";");
            } else if (ValidateNotNull.class.isInstance(annotation)) {
                rule.append(((ValidateNotNull) annotation).message()).append(";");
            } else if (ValidatePattern.class.isInstance(annotation)) {
                rule.append(((ValidatePattern) annotation).message()).append(";");
            } else if (ValidateStringIn.class.isInstance(annotation)) {
                rule.append(((ValidateStringIn) annotation).message()).append(";");
            }
        }
        return rule.toString();
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        int isDoc = Integer.valueOf(request.getParameter("doc") == null ? "0" : request.getParameter("doc"));

        if (isDoc == 2) {

            StringBuilder logMsg = new StringBuilder("\nAPI doc handle report -------- " + DateKit.currentDatetime() + " -----------------------------------------");
            logMsg.append("\nURI         : ").append(request.getRequestURI()).append(", Method : ").append(request.getMethod());

            if (handler instanceof HandlerMethod) {
                logMsg.append("\nController  : ").append(((HandlerMethod) handler).getBeanType().getName()).append(", Method : ").append(((HandlerMethod) handler).getMethod().getName());
            }

            if (request.getMethod().equalsIgnoreCase("GET")) {
                logMsg.append("\nQueryString : ").append(URLDecoder.decode((request.getQueryString() == null ? "null" : request.getQueryString()), CharsetKit.DEFAULT_ENCODE));
            } else if (request.getMethod().equalsIgnoreCase("POST")) {
                logMsg.append("\nParameter   : ").append(request.getParameterMap());
            }

            List<ResParam> resParams = new ArrayList<>();
            ModelResult result = (ModelResult) request.getAttribute("result");
            if (ex != null) {
                if (result == null) {
                    result = new ModelResult(ModelResult.CODE_500);
                } else {
                    result.setCode(ModelResult.CODE_500);
                }

                if (ex instanceof IllegalArgumentException) {
                    result.setCode(400);
                    result.setMessage(ex.getMessage());
                } else if (ex instanceof UndeclaredThrowableException) {
                    result.setMessage(((UndeclaredThrowableException) ex).getUndeclaredThrowable().getMessage());
                } else {
                    result.setMessage(ex.getMessage());
                }
            }

            long index = 0;

            ResParam codeParam = new ResParam(++index, "code", String.valueOf(result.getCode()), "HTTP状态", "Integer");
            resParams.add(codeParam);

            ResParam dataParam = new ResParam(++index, "data", StringUtils.EMPTY, "响应数据", StringUtils.EMPTY);
            resParams.add(dataParam);

            List<ResParam> children = new ArrayList<>();
            dataParam.setChildren(children);

            ResParam msgParam = new ResParam(++index, "message", result.getMessage(), "处理信息", "String");
            children.add(msgParam);

            if (result.getEntity() != null) {
                Object obj = result.getEntity();

                ResParam entityParam = new ResParam(++index, "entity", StringUtils.EMPTY, "实体对象", obj.getClass().getSimpleName());
                children.add(entityParam);

                List<ResParam> entityChildren = new ArrayList<>();
                entityParam.setChildren(entityChildren);

                List<Field> fieldList = getFields(obj.getClass());

                for (Field field : fieldList) {
                    index = buildFieldParams(index, obj, entityChildren, field);
                }
            } else if (CollectionUtils.isNotEmpty(result.getList())) {
                if (result.getRecordCount() > 0) {
                    ResParam recordCountParam = new ResParam(++index, "recordCount", String.valueOf(result.getRecordCount()), "当前页记录数", "Integer");
                    children.add(recordCountParam);
                }

                if (result.getTotalCount() > 0) {
                    ResParam totalParam = new ResParam(++index, "totalCount", String.valueOf(result.getTotalCount()), "总记录数", "Integer");
                    children.add(totalParam);
                }

                if (result.getPageNo() > 0) {
                    ResParam pageNoParam = new ResParam(++index, "pageNo", String.valueOf(result.getPageNo()), "当前页码", "Integer");
                    children.add(pageNoParam);
                }

                if (result.getTotalPage() > 0) {
                    ResParam totalPageParam = new ResParam(++index, "totalPage", String.valueOf(result.getTotalPage()), "总页数", "Integer");
                    children.add(totalPageParam);
                }

                if (result.getPageSize() > 0) {
                    ResParam sizeParam = new ResParam(++index, "pageSize", String.valueOf(result.getPageSize()), "每页大小", "Integer");
                    children.add(sizeParam);
                }

                List items = result.getList();

                ResParam listParam = new ResParam(++index, "list", StringUtils.EMPTY, "集合对象", "List");
                children.add(listParam);

                List<ResParam> listChildren = new ArrayList<>();
                listParam.setChildren(listChildren);

                for (int j = 0; j < items.size(); j++) {
                    ResParam listItem = new ResParam(++index, "[" + j + "]", StringUtils.EMPTY, StringUtils.EMPTY, items.get(j).getClass().getSimpleName());
                    listChildren.add(listItem);

                    List<ResParam> itemChildren = new ArrayList<>();
                    listItem.setChildren(itemChildren);

                    List<Field> fieldList = getFields(items.get(j).getClass());

                    for (Field field : fieldList) {
                        index = buildFieldParams(index, items.get(j), itemChildren, field);
                    }
                }
            }

            ModelResult modelResult = responseList(ModelResult.CODE_200, ModelResult.SUCCESS, resParams);

            logMsg.append("\nResponse    : ").append(JsonKit.toJson(modelResult));
            logMsg.append("\nCost Time   : ").append(System.currentTimeMillis() - ThreadLocalKit.getTime()).append(" ms");
            logMsg.append("\n--------------------------------------------------------------------------------------------");
            log.info(logMsg.toString());

            if (response.getContentType() == null || !response.getContentType().equalsIgnoreCase("application/octet-stream")) {
                ResponseKit.renderJson(response, modelResult);
            }
        }
    }

    private long buildFieldParams(long index, Object obj, List<ResParam> children, Field field) {

        if (field.getName().equalsIgnoreCase("serialVersionUID")) {
            return index;
        }

        boolean isFlag = field.getType().equals(String.class)
            || field.getType().equals(Integer.class)
            || field.getType().equals(Long.class)
            || field.getType().equals(Double.class)
            || field.getType().equals(Float.class);
        InitField initField = field.getAnnotation(InitField.class);
        ResParam fieldParam = new ResParam(++index, field.getName(), StringUtils.EMPTY, initField == null ? StringUtils.EMPTY : initField.desc(), field.getType().getSimpleName());
        if (field.getType().equals(List.class)) {
            List fieldValue = (List) getFieldValue(obj, field.getName());
            if (fieldValue != null) {
                List<ResParam> fieldChildren = new ArrayList<>();
                fieldParam.setChildren(fieldChildren);

                Class genericClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                int i = 0;
                for (Object item : fieldValue) {
                    initField = item.getClass().getAnnotation(InitField.class);
                    ResParam itemParam = new ResParam(++index, "[" + i++ + "]", StringUtils.EMPTY, initField == null ? StringUtils.EMPTY : initField.desc(), genericClass.getSimpleName());
                    fieldChildren.add(itemParam);

                    if (item.getClass().equals(String.class)
                        || item.getClass().equals(Integer.class)
                        || item.getClass().equals(Long.class)
                        || item.getClass().equals(Double.class)
                        || item.getClass().equals(Float.class)) {
                        itemParam.setValue(item.toString());
                    } else {
                        List<ResParam> childrenParams = new ArrayList<>();
                        itemParam.setChildren(childrenParams);

                        List<Field> fieldList = getFields(item.getClass());

                        for (Field f : fieldList) {
                            index = buildFieldParams(index, item, childrenParams, f);
                        }
                    }
                }
            }
        } else if (field.getType().equals(Set.class)) {
            Set fieldValue = (Set) getFieldValue(obj, field.getName());
            if (fieldValue != null) {
                List<ResParam> fieldChildren = new ArrayList<>();
                fieldParam.setChildren(fieldChildren);

                Class genericClass = (Class) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                int i = 0;
                for (Object item : fieldValue) {
                    initField = item.getClass().getAnnotation(InitField.class);
                    ResParam itemParam = new ResParam(++index, "[" + i++ + "]", StringUtils.EMPTY, initField == null ? StringUtils.EMPTY : initField.desc(), genericClass.getSimpleName());
                    fieldChildren.add(itemParam);

                    if (item.getClass().equals(String.class)
                        || item.getClass().equals(Integer.class)
                        || item.getClass().equals(Long.class)
                        || item.getClass().equals(Double.class)
                        || item.getClass().equals(Float.class)) {
                        itemParam.setValue(item.toString());
                    } else {
                        List<ResParam> childrenParams = new ArrayList<>();
                        itemParam.setChildren(childrenParams);

                        List<Field> fieldList = getFields(item.getClass());
                        for (Field f : fieldList) {
                            index = buildFieldParams(index, item, childrenParams, f);
                        }
                    }
                }
            }
        } else if (field.getType().isArray()) {
            Object[] fieldValue = (Object[]) getFieldValue(obj, field.getName());
            if (fieldValue != null) {
                List<ResParam> fieldChildren = new ArrayList<>();
                fieldParam.setChildren(fieldChildren);

                Class genericClass = field.getType().getComponentType();
                int i = 0;
                for (Object item : fieldValue) {
                    initField = item.getClass().getAnnotation(InitField.class);
                    ResParam itemParam = new ResParam(++index, "[" + i++ + "]", StringUtils.EMPTY, initField == null ? StringUtils.EMPTY : initField.desc(), genericClass.getSimpleName());
                    fieldChildren.add(itemParam);

                    if (item.getClass().equals(String.class)
                        || item.getClass().equals(Integer.class)
                        || item.getClass().equals(Long.class)
                        || item.getClass().equals(Double.class)
                        || item.getClass().equals(Float.class)) {
                        itemParam.setValue(item.toString());
                    } else {
                        List<ResParam> childrenParams = new ArrayList<>();
                        itemParam.setChildren(childrenParams);

                        List<Field> fieldList = getFields(item.getClass());
                        for (Field f : fieldList) {
                            index = buildFieldParams(index, item, childrenParams, f);
                        }
                    }
                }
            }
        } else {
            if (!isFlag) {
                Object fieldValue = getFieldValue(obj, field.getName());
                if (fieldValue != null) {
                    List<ResParam> fieldChildren = new ArrayList<>();
                    fieldParam.setChildren(fieldChildren);

                    List<Field> fieldList = getFields(fieldValue.getClass());
                    for (Field f : fieldList) {
                        index = buildFieldParams(index, fieldValue, fieldChildren, f);
                    }
                }
            } else {
                fieldParam.setValue(String.valueOf(getFieldValue(obj, field.getName())));
            }
        }
        children.add(fieldParam);
        return index;
    }

    private List<Field> getFields(Class clazz) {
        List<Field> fieldList = new ArrayList<>();
        fieldList.addAll(Arrays.asList(clazz.getDeclaredFields()));
        return fieldList;
    }

    /**
     * 处理响应单个实体
     *
     * @param code
     * @param message
     * @param entity
     * @return
     */
    protected final ModelResult responseEntity(int code, String message, Object entity) {
        ModelResult modelResult = new ModelResult(code);
        modelResult.setMessage(message);
        modelResult.setEntity(entity);

        return modelResult;
    }

    /**
     * 处理响应list
     *
     * @param code
     * @param message
     * @param list
     * @return
     */
    protected final ModelResult responseList(int code, String message, List list) {
        ModelResult modelResult = new ModelResult(code);
        modelResult.setMessage(message);
        modelResult.setList(list);

        return modelResult;
    }

    private Object getFieldValue(Object obj, String field) {
        String firstLetter = field.substring(0, 1).toUpperCase();
        String getMethodName = "get" + firstLetter + field.substring(1);
        Method getMethod;
        try {
            getMethod = obj.getClass().getMethod(getMethodName);
            return getMethod.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
            return StringUtils.EMPTY;
        }
    }
}
