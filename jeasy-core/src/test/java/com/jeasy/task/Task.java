package com.jeasy.task;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * Spring task 定时任务测试，适用于单系统
 * 注意：不适合用于集群
 */
@Component
public class Task {

    @Scheduled(cron = "0 * * * * ?")
    public void cronTest() {
        System.out.println("cron task execute");
    }
}
