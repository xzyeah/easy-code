package com.jeasy.task;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * spring task测试
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("/task/spring-task.xml")
public class TaskTest {

    @Test
    public void task() throws InterruptedException {
        while (true) {
            Thread.sleep(1000);
        }
    }
}
