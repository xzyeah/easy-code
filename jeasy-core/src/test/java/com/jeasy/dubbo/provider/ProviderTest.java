package com.jeasy.dubbo.provider;

import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 服务生产者测试
 */
public class ProviderTest {

    @SuppressWarnings("resource")
    public static void main(String[] args) throws Exception {
        new ClassPathXmlApplicationContext("/dubbo/spring-dubbo-provider.xml").start();

        System.in.read(); // 按任意键退出
    }
}
