package com.jeasy.dubbo;

/**
 * 用户服务
 */
public interface IUserService {

    public String getUserById(Long userId);
}
