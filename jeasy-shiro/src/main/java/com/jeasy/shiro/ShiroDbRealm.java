package com.jeasy.shiro;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Sets;
import com.jeasy.base.web.dto.CurrentUser;
import com.jeasy.common.collection.CollectionKit;
import com.jeasy.common.str.StrKit;
import com.jeasy.resource.dto.ResourceDTO;
import com.jeasy.resource.service.ResourceService;
import com.jeasy.roleresource.dto.RoleResourceDTO;
import com.jeasy.roleresource.service.RoleResourceService;
import com.jeasy.user.dto.UserDTO;
import com.jeasy.user.service.UserService;
import com.jeasy.userrole.dto.UserRoleDTO;
import com.jeasy.userrole.service.UserRoleService;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.CredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

import java.util.List;
import java.util.Set;

/**
 * shiro权限认证
 */
@Slf4j
public class ShiroDbRealm extends AuthorizingRealm {

    @Lazy
    @Autowired
    private UserService userService;

    @Lazy
    @Autowired
    private UserRoleService userRoleService;

    @Lazy
    @Autowired
    private RoleResourceService roleResourceService;

    @Lazy
    @Autowired
    private ResourceService resourceService;

    public ShiroDbRealm(CacheManager cacheManager, CredentialsMatcher matcher) {
        super(cacheManager, matcher);
    }

    /**
     * Shiro登录认证(原理：用户提交 用户名和密码  --- shiro 封装令牌 ---- realm 通过用户名将密码查询返回 ---- shiro 自动去比较查询出密码和用户输入密码是否一致---- 进行登陆控制 )
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authcToken;
        UserDTO userDTO = new UserDTO();
        userDTO.setName(token.getUsername());
        UserDTO user = userService.findOne(userDTO);
        // 账号不存在
        if (user == null) {
            throw new UnknownAccountException();
        }

        // 账号未启用
        if (user.getStatusVal() == 1001) {
            throw new DisabledAccountException();
        }

        UserRoleDTO userRoleDTO = new UserRoleDTO();
        userRoleDTO.setUserId(user.getId());

        List<UserRoleDTO> userRoleDTOs = userRoleService.find(userRoleDTO);
        Set<Long> roleIdSet = Sets.newHashSet();
        Set<String> roleNameSet = Sets.newHashSet();

        List<ResourceDTO> resourceDTOs = Lists.newArrayList();
        for (UserRoleDTO dto : userRoleDTOs) {
            if (dto.getRoleId() != null && dto.getRoleId() != 0L) {
                roleIdSet.add(dto.getRoleId());
            }

            if (!StrKit.isBlank(dto.getRoleCode())) {
                roleNameSet.add(dto.getRoleCode());
            }

            RoleResourceDTO roleResourceDTO = new RoleResourceDTO();
            roleResourceDTO.setRoleId(dto.getRoleId());
            List<RoleResourceDTO> dtos = roleResourceService.find(roleResourceDTO);
            List<Long> resourceIds = Lists.newArrayList();
            for (RoleResourceDTO roleResource : dtos) {
                resourceIds.add(roleResource.getResourceId());
            }

            List<ResourceDTO> resources = resourceService.findByIds(resourceIds);
            if (CollectionKit.isNotEmpty(resources)) {
                resourceDTOs.addAll(resources);
            }
        }

        Set<Long> operationIdSet = Sets.newHashSet();
        Set<String> operationUrlSet = Sets.newHashSet();

        for (ResourceDTO resourceDTO : resourceDTOs) {
            if (resourceDTO.getId() != null && resourceDTO.getId() != 0L) {
                operationIdSet.add(resourceDTO.getId());
            }

            if (!StrKit.isBlank(resourceDTO.getCode())) {
                operationUrlSet.add(resourceDTO.getCode());
            }
        }

        CurrentUser currentUser = new CurrentUser();
        currentUser.setId(user.getId());
        currentUser.setName(user.getName());
        currentUser.setLoginName(token.getUsername());
        currentUser.setCode(user.getCode());
        currentUser.setIsTest(user.getIsTest());
        currentUser.setDataSecurityVal(user.getDataSecurityVal());

        currentUser.setRoleIdSet(roleIdSet);
        currentUser.setRoleNameSet(roleNameSet);

        currentUser.setOperationIdSet(operationIdSet);
        currentUser.setOperationUrlSet(operationUrlSet);

        // 认证缓存信息
        return new SimpleAuthenticationInfo(currentUser, user.getPwd().toCharArray(), ShiroByteSource.of(user.getSalt()), getName());
    }

    /**
     * Shiro权限认证
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        CurrentUser currentUser = (CurrentUser) principals.getPrimaryPrincipal();

        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.setRoles(currentUser.getRoleNameSet());
        info.addStringPermissions(currentUser.getOperationUrlSet());

        return info;
    }

    @Override
    public void onLogout(PrincipalCollection principals) {
        super.clearCachedAuthorizationInfo(principals);
        CurrentUser user = (CurrentUser) principals.getPrimaryPrincipal();
        removeUserCache(user);
    }

    /**
     * 清除用户缓存
     *
     * @param user
     */
    public void removeUserCache(CurrentUser user) {
        removeUserCache(user.getLoginName());
    }

    /**
     * 清除用户缓存
     *
     * @param loginName
     */
    public void removeUserCache(String loginName) {
        SimplePrincipalCollection principals = new SimplePrincipalCollection();
        principals.add(loginName, super.getName());
        super.clearCachedAuthenticationInfo(principals);
    }
}
