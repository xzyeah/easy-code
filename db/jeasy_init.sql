CREATE DATABASE `jeasy` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

USE `jeasy`;
--
-- 用户管理-用户
--
DROP TABLE IF EXISTS `su_user`;
CREATE TABLE `su_user` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `login_name` VARCHAR(64) NOT NULL COMMENT '登陆名',
  `code` VARCHAR(64) DEFAULT '' COMMENT '编码',
  `pwd` VARCHAR(64) NOT NULL COMMENT '密码',
  `salt` VARCHAR(64) DEFAULT '' COMMENT '加密盐',
  `mobile` VARCHAR(11) DEFAULT '' COMMENT '手机号',
  `id_card_no` VARCHAR(18) DEFAULT '' COMMENT '身份证号',

  `status_val` INT(4) DEFAULT 0 COMMENT '用户状态值:1000=启用,1001=停用',
  `status_code` VARCHAR(16) DEFAULT '' COMMENT '用户状态编码:字典',

  `data_security_val` INT(4) DEFAULT 0 COMMENT '权限类型值:1010=当前用户,1011=所属机构,1012=所有数据',
  `data_security_code` VARCHAR(16) DEFAULT '' COMMENT '权限类型编码:字典',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户';

--
-- 用户管理-用户角色
--
DROP TABLE IF EXISTS `su_user_role`;
CREATE TABLE `su_user_role` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `user_id` BIGINT(8) NOT NULL COMMENT '用户ID',
  `user_name` VARCHAR(32) DEFAULT '' COMMENT '用户名称',
  `user_code` VARCHAR(64) DEFAULT '' COMMENT '用户编码',

  `role_id` BIGINT(8) NOT NULL COMMENT '角色ID',
  `role_name` VARCHAR(32) DEFAULT '' COMMENT '角色名称',
  `role_code` VARCHAR(64) DEFAULT '' COMMENT '角色编码',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户角色';

--
-- 用户管理-用户机构
--
DROP TABLE IF EXISTS `su_user_org`;
CREATE TABLE `su_user_org` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `user_id` BIGINT(8) NOT NULL COMMENT '用户ID',
  `user_name` VARCHAR(32) DEFAULT '' COMMENT '用户名称',
  `user_code` VARCHAR(64) DEFAULT '' COMMENT '用户标示',

  `org_id` BIGINT(8) NOT NULL COMMENT '机构ID',
  `org_name` VARCHAR(32) DEFAULT '' COMMENT '机构名称',
  `org_code` VARCHAR(64) DEFAULT '' COMMENT '机构编码',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='用户机构';


--
-- 权限管理-角色
--
DROP TABLE IF EXISTS `su_role`;
CREATE TABLE `su_role` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(64) DEFAULT '' COMMENT '编码',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(45) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(45) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色';

--
-- 权限管理-资源(菜单&操作)
--
DROP TABLE IF EXISTS `su_resource`;
CREATE TABLE `su_resource` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(32) DEFAULT '' COMMENT '编码',
  `url` VARCHAR(128) DEFAULT '' COMMENT 'URL',
  `icon` VARCHAR(64) DEFAULT '' COMMENT '图标',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注/描述',

  `pid` BIGINT(8) DEFAULT 0 COMMENT '父ID',
  `sort` TINYINT(1) DEFAULT 0 COMMENT '排序',
  `is_menu` TINYINT(1) NOT NULL COMMENT '是否菜单:0=否,1=是',
  `is_menu_leaf` TINYINT(1) NOT NULL COMMENT '是否叶子节点:0=否,1=是',
  `is_data_security` TINYINT(1) NOT NULL COMMENT '是否数据权限:0=否,1=是',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(45) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(45) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='菜单';

--
-- 权限管理-角色资源
--
DROP TABLE IF EXISTS `su_role_resource`;
CREATE TABLE `su_role_resource` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `role_id` BIGINT(8) NOT NULL COMMENT '角色ID',
  `role_name` VARCHAR(32) DEFAULT '' COMMENT '角色名称',
  `role_code` VARCHAR(64) DEFAULT '' COMMENT '角色编码',

  `resource_id` BIGINT(8) NOT NULL COMMENT '资源ID',
  `resource_name` VARCHAR(32) DEFAULT '' COMMENT '资源名称',
  `resource_code` VARCHAR(64) DEFAULT '' COMMENT '资源编码',

  `is_menu` TINYINT(1) NOT NULL COMMENT '是否菜单:0=否,1=是',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(45) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(45) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='角色资源';

--
-- 权限管理-机构
--
DROP TABLE IF EXISTS `su_organization`;
CREATE TABLE `su_organization` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(64) DEFAULT '' COMMENT '编码',
  `address` VARCHAR(128) DEFAULT '' COMMENT '地址',

  `type_val` INT(4) DEFAULT 0 COMMENT '机构类型值',
  `type_code` VARCHAR(16) DEFAULT '' COMMENT '机构类型编码:字典',

  `sort` TINYINT(1) DEFAULT 0 COMMENT '排序',
  `is_leaf` TINYINT(1) NOT NULL COMMENT '是否叶子节点:0=否,1=是',
  `pid` BIGINT(8) DEFAULT 0 COMMENT '父ID',
  `icon` VARCHAR(128) DEFAULT '' COMMENT '图标',

  `remark` VARCHAR(64) DEFAULT '' COMMENT '备注/描述',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(45) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(45) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='机构';

--
-- 基础数据-日志
--
DROP TABLE IF EXISTS `bd_log`;
CREATE TABLE `bd_log` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `table_name` VARCHAR(32) NOT NULL COMMENT '表名称',
  `record_id` BIGINT(8) NOT NULL COMMENT '记录ID',
  `field_name` VARCHAR(128) DEFAULT '' COMMENT '字段名称',

  `log_type_val` INT(4) DEFAULT 0 COMMENT '日志类型值:3020=定价日志,3021=信审日志,3022=普通日志',
  `log_type_code` VARCHAR(16) DEFAULT '' COMMENT '日志类型编码:字典',

  `opt_type_val` INT(4) DEFAULT 0 COMMENT '操作类型值:3030=创建,3031=删除,3032=修改,3033=拒绝,3034=通过,3035=回退',
  `opt_type_code` VARCHAR(16) DEFAULT '' COMMENT '操作类型编码:字典',
  `opt_desc` VARCHAR(128) DEFAULT '' COMMENT '操作类型描述',

  `before_value` VARCHAR(128) DEFAULT '' COMMENT '操作前值',
  `after_value` VARCHAR(128) DEFAULT '' COMMENT '操作后值',

  `remark` VARCHAR(256) DEFAULT '' COMMENT '备注',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='日志';

--
-- 基础数据-字典
--
DROP TABLE IF EXISTS `bd_dictionary`;
CREATE TABLE `bd_dictionary` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `name` VARCHAR(32) NOT NULL COMMENT '名称',
  `code` VARCHAR(16) NOT NULL COMMENT '编号',
  `value` INT(4) NOT NULL COMMENT '值',

  `type` VARCHAR(16) DEFAULT '' COMMENT '类型',
  `sort` TINYINT(1) DEFAULT 0 COMMENT '排序',

  `pid` BIGINT(8) DEFAULT 0 COMMENT '父ID',
  `pcode` VARCHAR(16) DEFAULT '' COMMENT '父编号',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=500 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='字典';

--
-- 基础数据-文件附件
--
DROP TABLE IF EXISTS `bd_file_attach`;
CREATE TABLE `bd_file_attach` (
  `id` BIGINT(8) NOT NULL AUTO_INCREMENT COMMENT '主键',

  `table_name` VARCHAR(32) NOT NULL COMMENT '表名称',
  `record_id` BIGINT(8) NOT NULL COMMENT '记录ID',

  `name` VARCHAR(64) DEFAULT '' COMMENT '文件原名称',
  `url` VARCHAR(256) DEFAULT '' COMMENT '文件URL',
  `icon_url` VARCHAR(256) DEFAULT '' COMMENT '文件图标URL',
  `preview_url` VARCHAR(256) DEFAULT '' COMMENT '文件预览URL',

  `create_at` BIGINT(8) DEFAULT 0 COMMENT '创建时间',
  `create_by` BIGINT(8) DEFAULT 0 COMMENT '创建人ID',
  `create_name` VARCHAR(32) DEFAULT '' COMMENT '创建人名称',
  `update_at` BIGINT(8) DEFAULT 0 COMMENT '更新时间',
  `update_by` BIGINT(8) DEFAULT 0 COMMENT '更新人ID',
  `update_name` VARCHAR(32) DEFAULT '' COMMENT '更新人名称',
  `is_del` TINYINT(1) DEFAULT 0 COMMENT '是否删除',
  `is_test` TINYINT(1) DEFAULT 0 COMMENT '是否测试',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='文件附件';

USE `jeasy`;
--
-- 初始化:字典
--
TRUNCATE `bd_dictionary`;

INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (1, '启用', 'QY', 1000, 'YHZT', 0, 0, '', 1494817962658, 0, 'SYSTEM', 1494817962658, 0, 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (2, '停用', 'TY', 1001, 'YHZT', 0, 0, '', 1494817962661, 0, 'SYSTEM', 1494817962661, 0, 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (3, '当前用户', 'DQYH', 1010, 'QXLX', 0, 0, '', 1494817962661, 0, 'SYSTEM', 1494817962661, 0, 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (4, '所属机构', 'SSJG', 1011, 'QXLX', 0, 0, '', 1494817962661, 0, 'SYSTEM', 1494817962661, 0, 'SYSTEM', 0, 0);
INSERT INTO `bd_dictionary` (`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (5, '所有数据', 'SYSJ', 1012, 'QXLX', 0, 0, '', 1494817962661, 0, 'SYSTEM', 1494817962661, 0, 'SYSTEM', 0, 0);

--
-- 初始化:菜单资源+角色资源
--
TRUNCATE `su_resource`;
TRUNCATE `su_role_resource`;

INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (1, '配置管理', 'PZGL', '', '', '', 0, 0, 1, 0, 0, 1492679052641, 0, 'SYSTEM', 1492679052641, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (2, '审批配置', 'PZGL_SPPZ', '', '', '', 1, 0, 1, 1, 0, 1492679052655, 0, 'SYSTEM', 1492679052655, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (1, 1, '超级管理员', 'CJGLY', 2, '审批配置', 'PZGL_SPPZ', 1, 1492679052655, 0, 'SYSTEM', 1492679052655, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (3, '服务部配置', 'FWBPZ', '', '', '', 2, 0, 0, 0, 0, 1492679052655, 0, 'SYSTEM', 1492679052655, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (4, '查询', 'SPPZ_FWBPZ_CX', '', '', '', 3, 0, 0, 0, 0, 1492679052655, 0, 'SYSTEM', 1492679052655, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (2, 1, '超级管理员', 'CJGLY', 4, '查询', 'SPPZ_FWBPZ_CX', 0, 1492679052656, 0, 'SYSTEM', 1492679052656, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (5, '配置', 'SPPZ_FWBPZ_PZ', '', '', '', 3, 0, 0, 0, 0, 1492679052656, 0, 'SYSTEM', 1492679052656, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (3, 1, '超级管理员', 'CJGLY', 5, '配置', 'SPPZ_FWBPZ_PZ', 0, 1492679052656, 0, 'SYSTEM', 1492679052656, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (6, '审批链配置', 'SPLPZ', '', '', '', 2, 0, 0, 0, 0, 1492679052656, 0, 'SYSTEM', 1492679052656, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (7, '查询', 'SPPZ_SPLPZ_CX', '', '', '', 6, 0, 0, 0, 0, 1492679052656, 0, 'SYSTEM', 1492679052656, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (4, 1, '超级管理员', 'CJGLY', 7, '查询', 'SPPZ_SPLPZ_CX', 0, 1492679052656, 0, 'SYSTEM', 1492679052656, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (8, '配置', 'SPPZ_SPLPZ_PZ', '', '', '', 6, 0, 0, 0, 0, 1492679052657, 0, 'SYSTEM', 1492679052657, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (5, 1, '超级管理员', 'CJGLY', 8, '配置', 'SPPZ_SPLPZ_PZ', 0, 1492679052657, 0, 'SYSTEM', 1492679052657, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (9, '业务配置', 'PZGL_YWPZ', '', '', '', 1, 0, 1, 1, 0, 1492679052657, 0, 'SYSTEM', 1492679052657, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (6, 1, '超级管理员', 'CJGLY', 9, '业务配置', 'PZGL_YWPZ', 1, 1492679052657, 0, 'SYSTEM', 1492679052657, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (10, '拒绝原因配置', 'JJYYPZ', '', '', '', 9, 0, 0, 0, 0, 1492679052657, 0, 'SYSTEM', 1492679052657, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (11, '新增', 'YWPZ_JJYYPZ_XZ', '', '', '', 10, 0, 0, 0, 0, 1492679052658, 0, 'SYSTEM', 1492679052658, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (7, 1, '超级管理员', 'CJGLY', 11, '新增', 'YWPZ_JJYYPZ_XZ', 0, 1492679052658, 0, 'SYSTEM', 1492679052658, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (12, '删除', 'YWPZ_JJYYPZ_SC', '', '', '', 10, 0, 0, 0, 0, 1492679052658, 0, 'SYSTEM', 1492679052658, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (8, 1, '超级管理员', 'CJGLY', 12, '删除', 'YWPZ_JJYYPZ_SC', 0, 1492679052658, 0, 'SYSTEM', 1492679052658, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (13, '回退原因配置', 'HTYYPZ', '', '', '', 9, 0, 0, 0, 0, 1492679052658, 0, 'SYSTEM', 1492679052658, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (14, '新增', 'YWPZ_HTYYPZ_XZ', '', '', '', 13, 0, 0, 0, 0, 1492679052658, 0, 'SYSTEM', 1492679052658, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (9, 1, '超级管理员', 'CJGLY', 14, '新增', 'YWPZ_HTYYPZ_XZ', 0, 1492679052659, 0, 'SYSTEM', 1492679052659, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (15, '删除', 'YWPZ_HTYYPZ_SC', '', '', '', 13, 0, 0, 0, 0, 1492679052659, 0, 'SYSTEM', 1492679052659, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (10, 1, '超级管理员', 'CJGLY', 15, '删除', 'YWPZ_HTYYPZ_SC', 0, 1492679052659, 0, 'SYSTEM', 1492679052659, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (16, '黑名单配置', 'HMDPZ', '', '', '', 9, 0, 0, 0, 0, 1492679052659, 0, 'SYSTEM', 1492679052659, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (17, '新增', 'YWPZ_HMDPZ_XZ', '', '', '', 16, 0, 0, 0, 0, 1492679052659, 0, 'SYSTEM', 1492679052659, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (11, 1, '超级管理员', 'CJGLY', 17, '新增', 'YWPZ_HMDPZ_XZ', 0, 1492679052660, 0, 'SYSTEM', 1492679052660, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (18, '删除', 'YWPZ_HMDPZ_SC', '', '', '', 16, 0, 0, 0, 0, 1492679052660, 0, 'SYSTEM', 1492679052660, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (12, 1, '超级管理员', 'CJGLY', 18, '删除', 'YWPZ_HMDPZ_SC', 0, 1492679052660, 0, 'SYSTEM', 1492679052660, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (19, '审批金额配置', 'PZGL_SPJEPZ', '', '', '', 1, 0, 1, 1, 0, 1492679052660, 0, 'SYSTEM', 1492679052660, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (13, 1, '超级管理员', 'CJGLY', 19, '审批金额配置', 'PZGL_SPJEPZ', 1, 1492679052661, 0, 'SYSTEM', 1492679052661, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (20, '确定', 'SPJEPZ_QD', '', '', '', 19, 0, 0, 0, 0, 1492679052661, 0, 'SYSTEM', 1492679052661, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (14, 1, '超级管理员', 'CJGLY', 20, '确定', 'SPJEPZ_QD', 0, 1492679052661, 0, 'SYSTEM', 1492679052661, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (21, '取消', 'SPJEPZ_QX', '', '', '', 19, 0, 0, 0, 0, 1492679052661, 0, 'SYSTEM', 1492679052661, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (15, 1, '超级管理员', 'CJGLY', 21, '取消', 'SPJEPZ_QX', 0, 1492679052662, 0, 'SYSTEM', 1492679052662, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (22, '授权配置', 'PZGL_SQPZ', '', '', '', 1, 0, 1, 1, 0, 1492679052662, 0, 'SYSTEM', 1492679052662, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (16, 1, '超级管理员', 'CJGLY', 22, '授权配置', 'PZGL_SQPZ', 1, 1492679052662, 0, 'SYSTEM', 1492679052662, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (23, '本人授权', 'BRSQ', '', '', '', 22, 0, 0, 0, 0, 1492679052662, 0, 'SYSTEM', 1492679052662, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (24, '查询', 'SQPZ_BRSQ_CX', '', '', '', 23, 0, 0, 0, 0, 1492679052663, 0, 'SYSTEM', 1492679052663, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (17, 1, '超级管理员', 'CJGLY', 24, '查询', 'SQPZ_BRSQ_CX', 0, 1492679052663, 0, 'SYSTEM', 1492679052663, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (25, '确定', 'SQPZ_BRSQ_QD', '', '', '', 23, 0, 0, 0, 0, 1492679052663, 0, 'SYSTEM', 1492679052663, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (18, 1, '超级管理员', 'CJGLY', 25, '确定', 'SQPZ_BRSQ_QD', 0, 1492679052663, 0, 'SYSTEM', 1492679052663, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (26, '取消', 'SQPZ_BRSQ_QX', '', '', '', 23, 0, 0, 0, 0, 1492679052664, 0, 'SYSTEM', 1492679052664, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (19, 1, '超级管理员', 'CJGLY', 26, '取消', 'SQPZ_BRSQ_QX', 0, 1492679052664, 0, 'SYSTEM', 1492679052664, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (27, '三方授权', 'SFSQ', '', '', '', 22, 0, 0, 0, 0, 1492679052664, 0, 'SYSTEM', 1492679052664, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (28, '查询', 'SQPZ_SFSQ_CX', '', '', '', 27, 0, 0, 0, 0, 1492679052664, 0, 'SYSTEM', 1492679052664, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (20, 1, '超级管理员', 'CJGLY', 28, '查询', 'SQPZ_SFSQ_CX', 0, 1492679052665, 0, 'SYSTEM', 1492679052665, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (29, '确定', 'SQPZ_SFSQ_QD', '', '', '', 27, 0, 0, 0, 0, 1492679052665, 0, 'SYSTEM', 1492679052665, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (21, 1, '超级管理员', 'CJGLY', 29, '确定', 'SQPZ_SFSQ_QD', 0, 1492679052665, 0, 'SYSTEM', 1492679052665, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (30, '取消', 'SQPZ_SFSQ_QX', '', '', '', 27, 0, 0, 0, 0, 1492679052665, 0, 'SYSTEM', 1492679052665, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (22, 1, '超级管理员', 'CJGLY', 30, '取消', 'SQPZ_SFSQ_QX', 0, 1492679052666, 0, 'SYSTEM', 1492679052666, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (31, '用户管理', 'YHGL', '', '', '', 0, 0, 1, 0, 0, 1492679052666, 0, 'SYSTEM', 1492679052666, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (32, '人员管理', 'YHGL_RYGL', '', '', '', 31, 0, 1, 1, 0, 1492679052666, 0, 'SYSTEM', 1492679052666, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (23, 1, '超级管理员', 'CJGLY', 32, '人员管理', 'YHGL_RYGL', 1, 1492679052666, 0, 'SYSTEM', 1492679052666, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (33, '查询', 'RYGL_CX', '', '', '', 32, 0, 0, 0, 0, 1492679052666, 0, 'SYSTEM', 1492679052666, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (24, 1, '超级管理员', 'CJGLY', 33, '查询', 'RYGL_CX', 0, 1492679052666, 0, 'SYSTEM', 1492679052666, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (34, '添加', 'RYGL_TJ', '', '', '', 32, 0, 0, 0, 0, 1492679052667, 0, 'SYSTEM', 1492679052667, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (25, 1, '超级管理员', 'CJGLY', 34, '添加', 'RYGL_TJ', 0, 1492679052667, 0, 'SYSTEM', 1492679052667, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (35, '修改', 'RYGL_XG', '', '', '', 32, 0, 0, 0, 0, 1492679052667, 0, 'SYSTEM', 1492679052667, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (26, 1, '超级管理员', 'CJGLY', 35, '修改', 'RYGL_XG', 0, 1492679052667, 0, 'SYSTEM', 1492679052667, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (36, '删除', 'RYGL_SC', '', '', '', 32, 0, 0, 0, 0, 1492679052667, 0, 'SYSTEM', 1492679052667, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (27, 1, '超级管理员', 'CJGLY', 36, '删除', 'RYGL_SC', 0, 1492679052667, 0, 'SYSTEM', 1492679052667, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (37, '密码重置', 'RYGL_MMZZ', '', '', '', 32, 0, 0, 0, 0, 1492679052667, 0, 'SYSTEM', 1492679052667, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (28, 1, '超级管理员', 'CJGLY', 37, '密码重置', 'RYGL_MMZZ', 0, 1492679052668, 0, 'SYSTEM', 1492679052668, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (38, '刷新', 'RYGL_SX', '', '', '', 32, 0, 0, 0, 0, 1492679052668, 0, 'SYSTEM', 1492679052668, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (29, 1, '超级管理员', 'CJGLY', 38, '刷新', 'RYGL_SX', 0, 1492679052668, 0, 'SYSTEM', 1492679052668, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (39, '配置', 'RYGL_PZ', '', '', '', 32, 0, 0, 0, 0, 1492679052668, 0, 'SYSTEM', 1492679052668, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (30, 1, '超级管理员', 'CJGLY', 39, '配置', 'RYGL_PZ', 0, 1492679052668, 0, 'SYSTEM', 1492679052668, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (40, '角色管理', 'YHGL_JSGL', '', '', '', 31, 0, 1, 1, 0, 1492679052668, 0, 'SYSTEM', 1492679052668, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (31, 1, '超级管理员', 'CJGLY', 40, '角色管理', 'YHGL_JSGL', 1, 1492679052668, 0, 'SYSTEM', 1492679052668, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (41, '查询', 'JSGL_CX', '', '', '', 40, 0, 0, 0, 0, 1492679052669, 0, 'SYSTEM', 1492679052669, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (32, 1, '超级管理员', 'CJGLY', 41, '查询', 'JSGL_CX', 0, 1492679052669, 0, 'SYSTEM', 1492679052669, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (42, '添加', 'JSGL_TJ', '', '', '', 40, 0, 0, 0, 0, 1492679052669, 0, 'SYSTEM', 1492679052669, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (33, 1, '超级管理员', 'CJGLY', 42, '添加', 'JSGL_TJ', 0, 1492679052669, 0, 'SYSTEM', 1492679052669, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (43, '修改', 'JSGL_XG', '', '', '', 40, 0, 0, 0, 0, 1492679052669, 0, 'SYSTEM', 1492679052669, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (34, 1, '超级管理员', 'CJGLY', 43, '修改', 'JSGL_XG', 0, 1492679052669, 0, 'SYSTEM', 1492679052669, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (44, '删除', 'JSGL_SC', '', '', '', 40, 0, 0, 0, 0, 1492679052669, 0, 'SYSTEM', 1492679052669, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (35, 1, '超级管理员', 'CJGLY', 44, '删除', 'JSGL_SC', 0, 1492679052670, 0, 'SYSTEM', 1492679052670, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (45, '刷新', 'JSGL_SX', '', '', '', 40, 0, 0, 0, 0, 1492679052670, 0, 'SYSTEM', 1492679052670, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (36, 1, '超级管理员', 'CJGLY', 45, '刷新', 'JSGL_SX', 0, 1492679052670, 0, 'SYSTEM', 1492679052670, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (46, '配置', 'JSGL_PZ', '', '', '', 40, 0, 0, 0, 0, 1492679052670, 0, 'SYSTEM', 1492679052670, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (37, 1, '超级管理员', 'CJGLY', 46, '配置', 'JSGL_PZ', 0, 1492679052670, 0, 'SYSTEM', 1492679052670, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (47, '机构管理', 'YHGL_JGGL', '', '', '', 31, 0, 1, 1, 0, 1492679052670, 0, 'SYSTEM', 1492679052670, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (38, 1, '超级管理员', 'CJGLY', 47, '机构管理', 'YHGL_JGGL', 1, 1492679052670, 0, 'SYSTEM', 1492679052670, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (48, '添加', 'JGGL_TJ', '', '', '', 47, 0, 0, 0, 0, 1492679052671, 0, 'SYSTEM', 1492679052671, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (39, 1, '超级管理员', 'CJGLY', 48, '添加', 'JGGL_TJ', 0, 1492679052671, 0, 'SYSTEM', 1492679052671, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (49, '修改', 'JGGL_XG', '', '', '', 47, 0, 0, 0, 0, 1492679052671, 0, 'SYSTEM', 1492679052671, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (40, 1, '超级管理员', 'CJGLY', 49, '修改', 'JGGL_XG', 0, 1492679052671, 0, 'SYSTEM', 1492679052671, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (50, '删除', 'JGGL_SC', '', '', '', 47, 0, 0, 0, 0, 1492679052671, 0, 'SYSTEM', 1492679052671, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (41, 1, '超级管理员', 'CJGLY', 50, '删除', 'JGGL_SC', 0, 1492679052671, 0, 'SYSTEM', 1492679052671, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (51, '菜单权限', 'YHGL_CDQX', '', '', '', 31, 0, 1, 1, 0, 1492679052672, 0, 'SYSTEM', 1492679052672, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (42, 1, '超级管理员', 'CJGLY', 51, '菜单权限', 'YHGL_CDQX', 1, 1492679052672, 0, 'SYSTEM', 1492679052672, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (52, '添加', 'CDQX_TJ', '', '', '', 51, 0, 0, 0, 0, 1492679052672, 0, 'SYSTEM', 1492679052672, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (43, 1, '超级管理员', 'CJGLY', 52, '添加', 'CDQX_TJ', 0, 1492679052672, 0, 'SYSTEM', 1492679052672, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (53, '修改', 'CDQX_XG', '', '', '', 51, 0, 0, 0, 0, 1492679052672, 0, 'SYSTEM', 1492679052672, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (44, 1, '超级管理员', 'CJGLY', 53, '修改', 'CDQX_XG', 0, 1492679052672, 0, 'SYSTEM', 1492679052672, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (54, '删除', 'CDQX_SC', '', '', '', 51, 0, 0, 0, 0, 1492679052673, 0, 'SYSTEM', 1492679052673, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (45, 1, '超级管理员', 'CJGLY', 54, '删除', 'CDQX_SC', 0, 1492679052673, 0, 'SYSTEM', 1492679052673, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (55, '定价管理', 'DJGL', '', '', '', 0, 0, 1, 0, 0, 1492679052673, 0, 'SYSTEM', 1492679052673, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (56, '待分配列表', 'DJGL_DFPLB', '', '', '', 55, 0, 1, 1, 0, 1492679052673, 0, 'SYSTEM', 1492679052673, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (46, 1, '超级管理员', 'CJGLY', 56, '待分配列表', 'DJGL_DFPLB', 1, 1492679052673, 0, 'SYSTEM', 1492679052673, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (57, '查询', 'DFPLB_CX', '', '', '', 56, 0, 0, 0, 0, 1492679052673, 0, 'SYSTEM', 1492679052673, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (47, 1, '超级管理员', 'CJGLY', 57, '查询', 'DFPLB_CX', 0, 1492679052673, 0, 'SYSTEM', 1492679052673, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (58, '分配', 'DFPLB_FP', '', '', '', 56, 0, 0, 0, 0, 1492679052674, 0, 'SYSTEM', 1492679052674, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (48, 1, '超级管理员', 'CJGLY', 58, '分配', 'DFPLB_FP', 0, 1492679052674, 0, 'SYSTEM', 1492679052674, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (59, '全量分配', 'DFPLB_QLFP', '', '', '', 56, 0, 0, 0, 0, 1492679052674, 0, 'SYSTEM', 1492679052674, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (49, 1, '超级管理员', 'CJGLY', 59, '全量分配', 'DFPLB_QLFP', 0, 1492679052674, 0, 'SYSTEM', 1492679052674, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (60, '定价汇总表', 'DJGL_DJHZB', '', '', '', 55, 0, 1, 1, 0, 1492679052674, 0, 'SYSTEM', 1492679052674, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (50, 1, '超级管理员', 'CJGLY', 60, '定价汇总表', 'DJGL_DJHZB', 1, 1492679052674, 0, 'SYSTEM', 1492679052674, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (61, '查询', 'DJHZB_CX', '', '', '', 60, 0, 0, 0, 0, 1492679052675, 0, 'SYSTEM', 1492679052675, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (51, 1, '超级管理员', 'CJGLY', 61, '查询', 'DJHZB_CX', 0, 1492679052675, 0, 'SYSTEM', 1492679052675, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (62, '综合查询', 'DJHZB_ZHCX', '', '', '', 60, 0, 0, 0, 0, 1492679052675, 0, 'SYSTEM', 1492679052675, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (52, 1, '超级管理员', 'CJGLY', 62, '综合查询', 'DJHZB_ZHCX', 0, 1492679052675, 0, 'SYSTEM', 1492679052675, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (63, '查看', 'DJHZB_CK', '', '', '', 60, 0, 0, 0, 0, 1492679052675, 0, 'SYSTEM', 1492679052675, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (53, 1, '超级管理员', 'CJGLY', 63, '查看', 'DJHZB_CK', 0, 1492679052675, 0, 'SYSTEM', 1492679052675, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (64, '待定价列表', 'DJGL_DDJLB', '', '', '', 55, 0, 1, 1, 0, 1492679052676, 0, 'SYSTEM', 1492679052676, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (54, 1, '超级管理员', 'CJGLY', 64, '待定价列表', 'DJGL_DDJLB', 1, 1492679052676, 0, 'SYSTEM', 1492679052676, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (65, '查询', 'DDJLB_CX', '', '', '', 64, 0, 0, 0, 0, 1492679052676, 0, 'SYSTEM', 1492679052676, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (55, 1, '超级管理员', 'CJGLY', 65, '查询', 'DDJLB_CX', 0, 1492679052676, 0, 'SYSTEM', 1492679052676, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (66, '定价', 'DDJLB_DJ', '', '', '', 64, 0, 0, 0, 0, 1492679052676, 0, 'SYSTEM', 1492679052676, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (56, 1, '超级管理员', 'CJGLY', 66, '定价', 'DDJLB_DJ', 0, 1492679052677, 0, 'SYSTEM', 1492679052677, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (67, '查看', 'DDJLB_CK', '', '', '', 64, 0, 0, 0, 0, 1492679052677, 0, 'SYSTEM', 1492679052677, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (57, 1, '超级管理员', 'CJGLY', 67, '查看', 'DDJLB_CK', 0, 1492679052677, 0, 'SYSTEM', 1492679052677, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (68, '已定价列表', 'DJGL_YDJLB', '', '', '', 55, 0, 1, 1, 0, 1492679052677, 0, 'SYSTEM', 1492679052677, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (58, 1, '超级管理员', 'CJGLY', 68, '已定价列表', 'DJGL_YDJLB', 1, 1492679052678, 0, 'SYSTEM', 1492679052678, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (69, '查询', 'YDJLB_CX', '', '', '', 68, 0, 0, 0, 0, 1492679052678, 0, 'SYSTEM', 1492679052678, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (59, 1, '超级管理员', 'CJGLY', 69, '查询', 'YDJLB_CX', 0, 1492679052678, 0, 'SYSTEM', 1492679052678, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (70, '查看', 'YDJLB_CK', '', '', '', 68, 0, 0, 0, 0, 1492679052678, 0, 'SYSTEM', 1492679052678, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (60, 1, '超级管理员', 'CJGLY', 70, '查看', 'YDJLB_CK', 0, 1492679052678, 0, 'SYSTEM', 1492679052678, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (71, '定价页', 'DJGL_DJY', '', '', '', 55, 0, 1, 1, 0, 1492679052679, 0, 'SYSTEM', 1492679052679, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (61, 1, '超级管理员', 'CJGLY', 71, '定价页', 'DJGL_DJY', 1, 1492679052679, 0, 'SYSTEM', 1492679052679, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (72, '基本信息', 'DJY_JBXX', '', '', '', 71, 0, 0, 0, 0, 1492679052679, 0, 'SYSTEM', 1492679052679, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (62, 1, '超级管理员', 'CJGLY', 72, '基本信息', 'DJY_JBXX', 0, 1492679052679, 0, 'SYSTEM', 1492679052679, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (73, '承租人信息', 'DJY_CZRXX', '', '', '', 71, 0, 0, 0, 0, 1492679052680, 0, 'SYSTEM', 1492679052680, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (63, 1, '超级管理员', 'CJGLY', 73, '承租人信息', 'DJY_CZRXX', 0, 1492679052680, 0, 'SYSTEM', 1492679052680, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (74, '租赁物信息', 'DJY_ZLWXX', '', '', '', 71, 0, 0, 0, 0, 1492679052680, 0, 'SYSTEM', 1492679052680, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (64, 1, '超级管理员', 'CJGLY', 74, '租赁物信息', 'DJY_ZLWXX', 0, 1492679052680, 0, 'SYSTEM', 1492679052680, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (75, '配置检测', 'DJY_PZJC', '', '', '', 71, 0, 0, 0, 0, 1492679052680, 0, 'SYSTEM', 1492679052680, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (65, 1, '超级管理员', 'CJGLY', 75, '配置检测', 'DJY_PZJC', 0, 1492679052681, 0, 'SYSTEM', 1492679052681, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (76, '技术及工况检测', 'DJY_JSJGKJC', '', '', '', 71, 0, 0, 0, 0, 1492679052681, 0, 'SYSTEM', 1492679052681, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (66, 1, '超级管理员', 'CJGLY', 76, '技术及工况检测', 'DJY_JSJGKJC', 0, 1492679052681, 0, 'SYSTEM', 1492679052681, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (77, '担保信息', 'DJY_DBXX', '', '', '', 71, 0, 0, 0, 0, 1492679052681, 0, 'SYSTEM', 1492679052681, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (67, 1, '超级管理员', 'CJGLY', 77, '担保信息', 'DJY_DBXX', 0, 1492679052681, 0, 'SYSTEM', 1492679052681, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (78, '评估信息', 'DJY_PGXX', '', '', '', 71, 0, 0, 0, 0, 1492679052682, 0, 'SYSTEM', 1492679052682, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (68, 1, '超级管理员', 'CJGLY', 78, '评估信息', 'DJY_PGXX', 0, 1492679052682, 0, 'SYSTEM', 1492679052682, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (79, '三方数据', 'DJY_SFSJ', '', '', '', 71, 0, 0, 0, 0, 1492679052682, 0, 'SYSTEM', 1492679052682, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (69, 1, '超级管理员', 'CJGLY', 79, '三方数据', 'DJY_SFSJ', 0, 1492679052682, 0, 'SYSTEM', 1492679052682, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (80, '车鉴定', 'DJY_CJD', '', '', '', 71, 0, 0, 0, 0, 1492679052682, 0, 'SYSTEM', 1492679052682, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (70, 1, '超级管理员', 'CJGLY', 80, '车鉴定', 'DJY_CJD', 0, 1492679052682, 0, 'SYSTEM', 1492679052682, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (81, '定价', 'DJY_DJ', '', '', '', 71, 0, 0, 0, 0, 1492679052683, 0, 'SYSTEM', 1492679052683, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (71, 1, '超级管理员', 'CJGLY', 81, '定价', 'DJY_DJ', 0, 1492679052683, 0, 'SYSTEM', 1492679052683, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (82, '四项核查', 'DJY_SXHC', '', '', '', 71, 0, 0, 0, 0, 1492679052683, 0, 'SYSTEM', 1492679052683, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (72, 1, '超级管理员', 'CJGLY', 82, '四项核查', 'DJY_SXHC', 0, 1492679052683, 0, 'SYSTEM', 1492679052683, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (83, '信息审核', 'DJY_XXSH', '', '', '', 71, 0, 0, 0, 0, 1492679052683, 0, 'SYSTEM', 1492679052683, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (73, 1, '超级管理员', 'CJGLY', 83, '信息审核', 'DJY_XXSH', 0, 1492679052683, 0, 'SYSTEM', 1492679052683, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (84, '信审结论', 'DJY_XSJL', '', '', '', 71, 0, 0, 0, 0, 1492679052684, 0, 'SYSTEM', 1492679052684, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (74, 1, '超级管理员', 'CJGLY', 84, '信审结论', 'DJY_XSJL', 0, 1492679052684, 0, 'SYSTEM', 1492679052684, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (85, '轨迹', 'DJY_GJ', '', '', '', 71, 0, 0, 0, 0, 1492679052684, 0, 'SYSTEM', 1492679052684, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (75, 1, '超级管理员', 'CJGLY', 85, '轨迹', 'DJY_GJ', 0, 1492679052684, 0, 'SYSTEM', 1492679052684, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (86, '挂起', 'DJY_GQ', '', '', '', 71, 0, 0, 0, 0, 1492679052684, 0, 'SYSTEM', 1492679052684, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (76, 1, '超级管理员', 'CJGLY', 86, '挂起', 'DJY_GQ', 0, 1492679052684, 0, 'SYSTEM', 1492679052684, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (87, '提交决策', 'DJY_TJJC', '', '', '', 71, 0, 0, 0, 0, 1492679052685, 0, 'SYSTEM', 1492679052685, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (77, 1, '超级管理员', 'CJGLY', 87, '提交决策', 'DJY_TJJC', 0, 1492679052685, 0, 'SYSTEM', 1492679052685, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (88, '信审管理', 'XSGL', '', '', '', 0, 0, 1, 0, 0, 1492679052685, 0, 'SYSTEM', 1492679052685, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (89, '信审汇总表', 'XSGL_XSHZB', '', '', '', 88, 0, 1, 1, 0, 1492679052685, 0, 'SYSTEM', 1492679052685, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (78, 1, '超级管理员', 'CJGLY', 89, '信审汇总表', 'XSGL_XSHZB', 1, 1492679052685, 0, 'SYSTEM', 1492679052685, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (90, '查询', 'XSHZB_CX', '', '', '', 89, 0, 0, 0, 0, 1492679052685, 0, 'SYSTEM', 1492679052685, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (79, 1, '超级管理员', 'CJGLY', 90, '查询', 'XSHZB_CX', 0, 1492679052686, 0, 'SYSTEM', 1492679052686, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (91, '综合查询', 'XSHZB_ZHCX', '', '', '', 89, 0, 0, 0, 0, 1492679052686, 0, 'SYSTEM', 1492679052686, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (80, 1, '超级管理员', 'CJGLY', 91, '综合查询', 'XSHZB_ZHCX', 0, 1492679052686, 0, 'SYSTEM', 1492679052686, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (92, '查看', 'XSHZB_CK', '', '', '', 89, 0, 0, 0, 0, 1492679052686, 0, 'SYSTEM', 1492679052686, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (81, 1, '超级管理员', 'CJGLY', 92, '查看', 'XSHZB_CK', 0, 1492679052686, 0, 'SYSTEM', 1492679052686, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (93, '待审核列表', 'XSGL_DSHLB', '', '', '', 88, 0, 1, 1, 0, 1492679052686, 0, 'SYSTEM', 1492679052686, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (82, 1, '超级管理员', 'CJGLY', 93, '待审核列表', 'XSGL_DSHLB', 1, 1492679052687, 0, 'SYSTEM', 1492679052687, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (94, '查询', 'DSHLB_CX', '', '', '', 93, 0, 0, 0, 0, 1492679052687, 0, 'SYSTEM', 1492679052687, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (83, 1, '超级管理员', 'CJGLY', 94, '查询', 'DSHLB_CX', 0, 1492679052687, 0, 'SYSTEM', 1492679052687, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (95, '审核', 'DSHLB_SH', '', '', '', 93, 0, 0, 0, 0, 1492679052687, 0, 'SYSTEM', 1492679052687, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (84, 1, '超级管理员', 'CJGLY', 95, '审核', 'DSHLB_SH', 0, 1492679052687, 0, 'SYSTEM', 1492679052687, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (96, '查看', 'DSHLB_CK', '', '', '', 93, 0, 0, 0, 0, 1492679052688, 0, 'SYSTEM', 1492679052688, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (85, 1, '超级管理员', 'CJGLY', 96, '查看', 'DSHLB_CK', 0, 1492679052688, 0, 'SYSTEM', 1492679052688, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (97, '已审核列表', 'XSGL_YSHLB', '', '', '', 88, 0, 1, 1, 0, 1492679052688, 0, 'SYSTEM', 1492679052688, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (86, 1, '超级管理员', 'CJGLY', 97, '已审核列表', 'XSGL_YSHLB', 1, 1492679052688, 0, 'SYSTEM', 1492679052688, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (98, '查询', 'YSHLB_CX', '', '', '', 97, 0, 0, 0, 0, 1492679052688, 0, 'SYSTEM', 1492679052688, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (87, 1, '超级管理员', 'CJGLY', 98, '查询', 'YSHLB_CX', 0, 1492679052688, 0, 'SYSTEM', 1492679052688, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (99, '查看', 'YSHLB_CK', '', '', '', 97, 0, 0, 0, 0, 1492679052688, 0, 'SYSTEM', 1492679052688, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (88, 1, '超级管理员', 'CJGLY', 99, '查看', 'YSHLB_CK', 0, 1492679052688, 0, 'SYSTEM', 1492679052688, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (100, '审核页', 'XSGL_SHY', '', '', '', 88, 0, 1, 1, 0, 1492679052689, 0, 'SYSTEM', 1492679052689, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (89, 1, '超级管理员', 'CJGLY', 100, '审核页', 'XSGL_SHY', 1, 1492679052689, 0, 'SYSTEM', 1492679052689, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (101, '基本信息', 'SHY_JBXX', '', '', '', 100, 0, 0, 0, 0, 1492679052689, 0, 'SYSTEM', 1492679052689, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (90, 1, '超级管理员', 'CJGLY', 101, '基本信息', 'SHY_JBXX', 0, 1492679052689, 0, 'SYSTEM', 1492679052689, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (102, '承租人信息', 'SHY_CZRXX', '', '', '', 100, 0, 0, 0, 0, 1492679052689, 0, 'SYSTEM', 1492679052689, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (91, 1, '超级管理员', 'CJGLY', 102, '承租人信息', 'SHY_CZRXX', 0, 1492679052689, 0, 'SYSTEM', 1492679052689, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (103, '租赁物信息', 'SHY_ZLWXX', '', '', '', 100, 0, 0, 0, 0, 1492679052689, 0, 'SYSTEM', 1492679052689, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (92, 1, '超级管理员', 'CJGLY', 103, '租赁物信息', 'SHY_ZLWXX', 0, 1492679052690, 0, 'SYSTEM', 1492679052690, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (104, '配置检测', 'SHY_PZJC', '', '', '', 100, 0, 0, 0, 0, 1492679052690, 0, 'SYSTEM', 1492679052690, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (93, 1, '超级管理员', 'CJGLY', 104, '配置检测', 'SHY_PZJC', 0, 1492679052690, 0, 'SYSTEM', 1492679052690, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (105, '技术及工况检测', 'SHY_JSJGKJC', '', '', '', 100, 0, 0, 0, 0, 1492679052690, 0, 'SYSTEM', 1492679052690, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (94, 1, '超级管理员', 'CJGLY', 105, '技术及工况检测', 'SHY_JSJGKJC', 0, 1492679052690, 0, 'SYSTEM', 1492679052690, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (106, '担保信息', 'SHY_DBXX', '', '', '', 100, 0, 0, 0, 0, 1492679052690, 0, 'SYSTEM', 1492679052690, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (95, 1, '超级管理员', 'CJGLY', 106, '担保信息', 'SHY_DBXX', 0, 1492679052691, 0, 'SYSTEM', 1492679052691, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (107, '评估信息', 'SHY_PGXX', '', '', '', 100, 0, 0, 0, 0, 1492679052691, 0, 'SYSTEM', 1492679052691, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (96, 1, '超级管理员', 'CJGLY', 107, '评估信息', 'SHY_PGXX', 0, 1492679052691, 0, 'SYSTEM', 1492679052691, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (108, '三方数据', 'SHY_SFSJ', '', '', '', 100, 0, 0, 0, 0, 1492679052691, 0, 'SYSTEM', 1492679052691, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (97, 1, '超级管理员', 'CJGLY', 108, '三方数据', 'SHY_SFSJ', 0, 1492679052691, 0, 'SYSTEM', 1492679052691, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (109, '车鉴定', 'SHY_CJD', '', '', '', 100, 0, 0, 0, 0, 1492679052691, 0, 'SYSTEM', 1492679052691, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (98, 1, '超级管理员', 'CJGLY', 109, '车鉴定', 'SHY_CJD', 0, 1492679052691, 0, 'SYSTEM', 1492679052691, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (110, '定价', 'SHY_DJ', '', '', '', 100, 0, 0, 0, 0, 1492679052691, 0, 'SYSTEM', 1492679052691, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (99, 1, '超级管理员', 'CJGLY', 110, '定价', 'SHY_DJ', 0, 1492679052692, 0, 'SYSTEM', 1492679052692, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (111, '四项核查', 'SHY_SXHC', '', '', '', 100, 0, 0, 0, 0, 1492679052692, 0, 'SYSTEM', 1492679052692, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (100, 1, '超级管理员', 'CJGLY', 111, '四项核查', 'SHY_SXHC', 0, 1492679052692, 0, 'SYSTEM', 1492679052692, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (112, '信息审核', 'SHY_XXSH', '', '', '', 100, 0, 0, 0, 0, 1492679052692, 0, 'SYSTEM', 1492679052692, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (101, 1, '超级管理员', 'CJGLY', 112, '信息审核', 'SHY_XXSH', 0, 1492679052692, 0, 'SYSTEM', 1492679052692, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (113, '信审结论', 'SHY_XSJL', '', '', '', 100, 0, 0, 0, 0, 1492679052692, 0, 'SYSTEM', 1492679052692, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (102, 1, '超级管理员', 'CJGLY', 113, '信审结论', 'SHY_XSJL', 0, 1492679052692, 0, 'SYSTEM', 1492679052692, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (114, '轨迹', 'SHY_GJ', '', '', '', 100, 0, 0, 0, 0, 1492679052692, 0, 'SYSTEM', 1492679052692, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (103, 1, '超级管理员', 'CJGLY', 114, '轨迹', 'SHY_GJ', 0, 1492679052693, 0, 'SYSTEM', 1492679052693, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (115, '挂起', 'SHY_GQ', '', '', '', 100, 0, 0, 0, 0, 1492679052693, 0, 'SYSTEM', 1492679052693, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (104, 1, '超级管理员', 'CJGLY', 115, '挂起', 'SHY_GQ', 0, 1492679052693, 0, 'SYSTEM', 1492679052693, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (116, '提交决策', 'SHY_TJJC', '', '', '', 100, 0, 0, 0, 0, 1492679052693, 0, 'SYSTEM', 1492679052693, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (105, 1, '超级管理员', 'CJGLY', 116, '提交决策', 'SHY_TJJC', 0, 1492679052693, 0, 'SYSTEM', 1492679052693, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (117, '黑名单列表', 'XSGL_HMDLB', '', '', '', 88, 0, 1, 1, 0, 1492679052693, 0, 'SYSTEM', 1492679052693, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (106, 1, '超级管理员', 'CJGLY', 117, '黑名单列表', 'XSGL_HMDLB', 1, 1492679052693, 0, 'SYSTEM', 1492679052693, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (118, '查询', 'HMDLB_CX', '', '', '', 117, 0, 0, 0, 0, 1492679052693, 0, 'SYSTEM', 1492679052693, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (107, 1, '超级管理员', 'CJGLY', 118, '查询', 'HMDLB_CX', 0, 1492679052693, 0, 'SYSTEM', 1492679052693, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (119, '导出', 'HMDLB_DC', '', '', '', 117, 0, 0, 0, 0, 1492679052694, 0, 'SYSTEM', 1492679052694, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (108, 1, '超级管理员', 'CJGLY', 119, '导出', 'HMDLB_DC', 0, 1492679052694, 0, 'SYSTEM', 1492679052694, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (120, '导入', 'HMDLB_DR', '', '', '', 117, 0, 0, 0, 0, 1492679052694, 0, 'SYSTEM', 1492679052694, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (109, 1, '超级管理员', 'CJGLY', 120, '导入', 'HMDLB_DR', 0, 1492679052694, 0, 'SYSTEM', 1492679052694, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (121, '查看', 'HMDLB_CK', '', '', '', 117, 0, 0, 0, 0, 1492679052694, 0, 'SYSTEM', 1492679052694, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (110, 1, '超级管理员', 'CJGLY', 121, '查看', 'HMDLB_CK', 0, 1492679052694, 0, 'SYSTEM', 1492679052694, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (122, '删除', 'HMDLB_SC', '', '', '', 117, 0, 0, 0, 0, 1492679052694, 0, 'SYSTEM', 1492679052694, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (111, 1, '超级管理员', 'CJGLY', 122, '删除', 'HMDLB_SC', 0, 1492679052694, 0, 'SYSTEM', 1492679052694, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (123, '数据统计', 'SJTJ', '', '', '', 0, 0, 1, 0, 0, 1492679052694, 0, 'SYSTEM', 1492679052694, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (124, '审核信息表', 'SJTJ_SHXXB', '', '', '', 123, 0, 1, 1, 0, 1492679052695, 0, 'SYSTEM', 1492679052695, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (112, 1, '超级管理员', 'CJGLY', 124, '审核信息表', 'SJTJ_SHXXB', 1, 1492679052695, 0, 'SYSTEM', 1492679052695, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (125, '查询', 'SHXXB_CX', '', '', '', 124, 0, 0, 0, 0, 1492679052695, 0, 'SYSTEM', 1492679052695, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (113, 1, '超级管理员', 'CJGLY', 125, '查询', 'SHXXB_CX', 0, 1492679052695, 0, 'SYSTEM', 1492679052695, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_resource` (`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (126, '导出', 'SHXXB_DC', '', '', '', 124, 0, 0, 0, 0, 1492679052695, 0, 'SYSTEM', 1492679052695, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (114, 1, '超级管理员', 'CJGLY', 126, '导出', 'SHXXB_DC', 0, 1492679052695, 0, 'SYSTEM', 1492679052695, 0, 'SYSTEM', 0, 0);
--
-- 初始化:角色
--
TRUNCATE `su_role`;

INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (1, '超级管理员', 'CJGLY', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (2, '定价专员', 'DJZY', '', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (3, '定价主管', 'DJZG', '', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (4, '信审专员', 'XSZY', '', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (5, '信审主管', 'XSZG', '', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (6, '信审经理', 'XSJL', '', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (7, '信审总监', 'XSZJ', '', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_role` (`id`, `name`, `code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (8, '首席风控官', 'SXFKG', '', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

--
-- 初始化:机构
--
TRUNCATE `su_organization`;

INSERT INTO `su_organization` (`id`, `name`, `code`, `type_val`, `type_code`, `sort`, `is_leaf`, `pid`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (1, '超级管理组', 'CJGLZ', 2000, 'QT', 0, 1, 0, '谨慎操作',1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_organization` (`id`, `name`, `code`, `type_val`, `type_code`, `sort`, `is_leaf`, `pid`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (2, '定价组', 'DJZ', 2001, 'DJ', 0, 0, 0, '',1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_organization` (`id`, `name`, `code`, `type_val`, `type_code`, `sort`, `is_leaf`, `pid`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (3, '定价组一', 'DJZY', 2001, 'DJ', 0, 1, 2, '',1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_organization` (`id`, `name`, `code`, `type_val`, `type_code`, `sort`, `is_leaf`, `pid`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (4, '定价组二', 'DJZE', 2001, 'DJ', 0, 1, 2, '',1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_organization` (`id`, `name`, `code`, `type_val`, `type_code`, `sort`, `is_leaf`, `pid`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (5, '信审组', 'XSZ', 2002, 'XS', 0, 0, 0, '',1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_organization` (`id`, `name`, `code`, `type_val`, `type_code`, `sort`, `is_leaf`, `pid`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (6, '信审组一', 'XSZY', 2002, 'XS', 0, 1, 5, '谨慎操作',1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_organization` (`id`, `name`, `code`, `type_val`, `type_code`, `sort`, `is_leaf`, `pid`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (7, '信审组二', 'XSZE', 2002, 'XS', 0, 1, 5, '谨慎操作',1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

--
-- 初始化:用户+用户机构+用户角色
--
TRUNCATE `su_user`;

INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (1, 'admin', 'admin', 'ADMIN', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13011111111', '370134199910120678', 1000, 'QY', 1012, 'SYSJ', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (2, '张三', 'zhangsan', 'ZS', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13022222222', '370134199910120675', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (3, '李四', 'lisi', 'LS', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13033333333', '370134199910120676', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (4, '王五', 'wangwu', 'WW', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13044444444', '370134199910120675', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (5, '赵六', 'zhaoliu', 'ZL', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13055555555', '370134199910120675', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (6, '刘七', 'liuqi', 'LQ', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13066666666', '370134199910120677', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (7, '赵四', 'zhaosi', 'ZS', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13077777777', '370134199910120678', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (8, '刘能', 'liuneng', 'LN', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13088888888', '370134199910120679', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (9, '邦仁', 'bangren', 'BR', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13099999999', '370134199910120680', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (10, '李华', 'lihua', 'LH', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13155555555', '370134199910120775', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (11, '李凯', 'likai', 'LK', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13166666666', '370134199910120875', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (12, '王华', 'wanghua', 'WH', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13255555555', '370134199910140675', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (13, '刘壮', 'liuzhuang', 'LZ', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13266666666', '370134199910141675', 1000, 'QY', 1010, 'DQYH', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user` (`id`, `name`, `login_name`, `code`, `pwd`, `salt`, `mobile`, `id_card_no`, `status_val`, `status_code`, `data_security_val`, `data_security_code`, `remark`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (14, '李风', 'lifeng', 'LF', '7d61f71f34b0305aabc5d1cdd9d2a777', 'abc', '13888888888', '370134199910152675', 1000, 'QY', 1012, 'SYSJ', '谨慎操作', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);


TRUNCATE `su_user_org`;

INSERT INTO `su_user_org` (`id`, `user_id`, `user_name`, `user_code`, `org_id`, `org_name`, `org_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (1, 1, 'admin', 'ADMIN', 1, '超级管理组', 'CJGLZ', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_org` (`id`, `user_id`, `user_name`, `user_code`, `org_id`, `org_name`, `org_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (2, 2, '张三', 'ZS', 3, '定价组一', 'DJZY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_org` (`id`, `user_id`, `user_name`, `user_code`, `org_id`, `org_name`, `org_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (3, 3, '李四', 'LS', 3, '定价组一', 'DJZY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_org` (`id`, `user_id`, `user_name`, `user_code`, `org_id`, `org_name`, `org_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (4, 4, '王五', 'WW', 7, '信审组二', 'XSZE', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_org` (`id`, `user_id`, `user_name`, `user_code`, `org_id`, `org_name`, `org_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (5, 5, '赵六', 'ZL', 7, '信审组二', 'XSZE', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

INSERT INTO `su_user_org` (`id`, `user_id`, `user_name`, `user_code`, `org_id`, `org_name`, `org_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (6, 6, '刘七', 'LQ', 3, '定价组一', 'DJZY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_org` (`id`, `user_id`, `user_name`, `user_code`, `org_id`, `org_name`, `org_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (7, 7, '赵四', 'ZS', 4, '定价组二', 'DJZE', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_org` (`id`, `user_id`, `user_name`, `user_code`, `org_id`, `org_name`, `org_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (8, 8, '刘能', 'LN', 6, '信审组一', 'XSZY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_org` (`id`, `user_id`, `user_name`, `user_code`, `org_id`, `org_name`, `org_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (9, 9, '邦仁', 'BR', 7, '信审组二', 'XSZE', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);


TRUNCATE `su_user_role`;

INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES (1, 1, 'admin', 'ADMIN', 1, '超级管理员', 'CJGLY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (2, 2, '张三', 'ZS', 2, '定价专员', 'DJZY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (3, 3, '李四', 'LS', 2, '定价专员', 'DJZY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (4, 4, '王五', 'WW', 4, '信审专员', 'XSZY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (5, 5, '赵六', 'ZL', 4, '信审专员', 'XSZY', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (6, 6, '刘七', 'LQ', 3, '定价主管', 'DJZG', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (7, 7, '赵四', 'ZS', 3, '定价主管', 'DJZG', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (8, 8, '刘能', 'LN', 5, '信审主管', 'XSZG', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (9, 9, '邦仁', 'BR', 5, '信审主管', 'XSZG', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);

INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (10, 10, '李华', 'LH', 6, '信审经理', 'XSJL', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (11, 11, '李凯', 'LK', 6, '信审经理', 'XSJL', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (12, 12, '王华', 'WH', 7, '信审总监', 'XSZJ', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (13, 13, '刘壮', 'LZ', 7, '信审总监', 'XSZJ', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
INSERT INTO `su_user_role` (`id`, `user_id`, `user_name`, `user_code`, `role_id`, `role_name`, `role_code`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) VALUES
  (14, 14, '李风', 'LF', 8, '首席风控官', 'SXFKG', 1491805287470, 0, 'SYSTEM', 1491805287470, 0, 'SYSTEM', 0, 0);
