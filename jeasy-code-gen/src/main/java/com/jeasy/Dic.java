package com.jeasy;

import lombok.Data;

@Data
public class Dic {
    String name;

    String code;

    String origCode;
}
