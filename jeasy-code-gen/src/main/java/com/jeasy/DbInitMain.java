package com.jeasy;

import com.beust.jcommander.internal.Lists;
import com.beust.jcommander.internal.Maps;
import com.jeasy.common.charset.CharsetKit;
import com.jeasy.common.pinyin.PinYinKit;
import com.jeasy.common.str.StrKit;
import com.jeasy.common.template.TemplateKit;

import java.util.List;
import java.util.Map;

/**
 * @author TaoBangren
 * @version 1.0
 * @since 2017/4/8 下午3:00
 */
public class DbInitMain {
    public static void main(String[] args) {
//        initDictionary();
//        initResourcesAndRole();
//        initPriceCarInfo();
//        genDictionaryKit();
        genLogKit();
    }

    private static String params =
        "用户状态:1000=启用,1001=停用|" +
            "权限类型:1010=当前用户,1011=所属机构,1012=所有数据|" +
            "机构类型:2000=其他,2001=定价,2002=信审|" +
            "人员类型:3000=定价专员,3001=信审专员|" +
            "原因类型:3010=拒绝原因,3011=回退原因,3012=加黑原因|" +
            "日志类型:3020=定价日志,3021=信审日志,3022=普通日志|" +
            "操作类型:3030=创建,3031=删除,3032=修改,3033=拒绝,3034=通过,3035=回退,3037=登录,3038=登出|" +
            "授权类型:3040=本人授权,3041=三方授权|" +
            "本人授权查询条件:3050=用户名称,3051=用户手机|" +
            "三方授权查询条件:3060=授权人,3061=授权人角色,3062=授权人手机,3063=授予人,3064=授予人角色,3065=授予人手机|" +
            "定价状态:4000=未分配,4001=待定价,4002=定价中,4003=回退,4004=通过,4005=拒绝,4006=挂起(定价专员),4007=挂起(定价主管)," +
            "4008=客户放弃(回退),4009=服务部拒绝(回退),4300=进件超时(回退)," +
            "4301=客户放弃(通过),4302=服务部拒绝(通过),4303=进件超时(通过)|" +
            "待定价状态:4001=待定价,4002=定价中,4006=挂起(定价专员),4007=挂起(定价主管)|" +
            "已定价状态:4003=回退,4004=通过,4005=拒绝," +
            "4008=客户放弃(回退),4009=服务部拒绝(回退),4300=进件超时(回退)," +
            "4301=客户放弃(通过),4302=服务部拒绝(通过),4303=进件超时(通过)|" +
            "信审状态:" +
            "4021=待审核(信审专员),4022=审核中(信审专员)," +
            "4023=待审核(信审主管),4024=审核中(信审主管)," +
            "4025=待审核(信审经理),4026=审核中(信审经理)," +
            "4027=待审核(信审总监),4028=审核中(信审总监)," +
            "4029=待审核(首席风控官),4030=审核中(首席风控官)," +
            "4031=挂起(信审专员),4032=挂起(信审主管)," +
            "4033=挂起(信审经理),4034=挂起(信审总监)," +
            "4035=挂起(首席风控官)," +
            "4036=回退信审专员,4037=回退服务部,4038=信审通过,4039=信审拒绝," +
            "4200=进件超时(回退),4201=客户放弃(回退),4202=服务部拒绝(回退)," +
            "4203=进件超时(签约),4204=客户放弃(签约),4205=服务部拒绝(签约)|" +
            "待审核状态:" +
            "4021=待审核(信审专员),4022=审核中(信审专员)," +
            "4023=待审核(信审主管),4024=审核中(信审主管)," +
            "4025=待审核(信审经理),4026=审核中(信审经理)," +
            "4027=待审核(信审总监),4028=审核中(信审总监)," +
            "4029=待审核(首席风控官),4030=审核中(首席风控官)," +
            "4031=挂起(信审专员),4032=挂起(信审主管)," +
            "4033=挂起(信审经理),4034=挂起(信审总监)," +
            "4035=挂起(首席风控官)|" +
            "已审核状态:" +
            "4035=回退信审专员,4036=回退服务部,4037=信审通过,4038=信审拒绝," +
            "4200=进件超时(回退),4201=客户放弃(回退),4202=服务部拒绝(回退)," +
            "4203=进件超时(签约),4204=客户放弃(签约),4205=服务部拒绝(签约)|" +
            "节点状态:4040=未处理,4041=已处理,4042=已转配|" +
            "任务类型:4050=定价,4051=信审|" +
            "定价结论类型:4060=通过,4061=拒绝,4062=回退|" +
            "信审结论类型:4070=回退服务部,4071=回退信审专员,4072=通过,4073=拒绝|" +
            "加黑类型:4080=借款人身份证号,4081=借款人手机号,4082=配偶身份证号,4083=配偶手机号|" +
            "核查渠道:4090=工商核查,4091=网络核查,4092=人法核查,4093=失信核查,4094=其他备注,4095=资料核查,4096=征信核查,4097=银行流水,4098=电核情况及融资用途|" +
            "核查类型:4100=单位名称,4101=身份证号,4102=居住地址,4103=手机号码,4104=配偶手机,4105=家庭联系人,4106=其他联系人,4107=家庭电话,4108=单位电话,4109=配偶身份证号|" +
            "核查结果:4110=正常,4111=异常,4112=无信息,4113=齐全,4114=真实,4115=有效性|" +
            "待分配查询条件:4120=车辆评估编号,4121=评估师,4123=承租人,4124=手机号码,4125=身份证号|" +
            "定价汇总查询条件:4120=车辆评估编号,4121=评估师,4122=定价师,4123=承租人,4124=手机号码,4125=身份证号|" +
            "待定价查询条件:4120=车辆评估编号,4121=评估师,4123=承租人,4124=手机号码,4125=身份证号|" +
            "已定价查询条件:4120=车辆评估编号,4121=评估师,4122=定价师,4123=承租人,4124=手机号码,4125=身份证号|" +
            "信审汇总查询条件:4130=评估师,4131=定价师,4132=承租人,4133=手机号码,4134=身份证号|" +
            "待审核查询条件:4131=定价师,4132=承租人,4133=手机号码,4134=身份证号|" +
            "已审核查询条件:4131=定价师,4132=承租人,4133=手机号码,4134=身份证号|" +
            "定价建议类型:4130=正常,4131=偏高,4132=偏低|" +
            "信审通过条件:4140=正常,4141=持续关注GPS,4142=特殊复议通过,4143=评估价过高|" +
            "审批金额角色:4150=首席风控官,4151=信审总监,4152=信审经理,4153=信审主管,4154=信审专员|" +
            "角色类型:4150=首席风控官,4151=信审总监,4152=信审经理,4153=信审主管,4154=信审专员,4155=定价主管,4156=定价专员|" +
            "渠道类型:4010=普惠线下,4011=普惠新渠道,4012=普惠电商,4013=渠道销售,4014=渠道商,4015=代理商,4016=经销商|" +
            "产品类型:5010=产品类型A,5011=产品类型B";

    private static void genDictionaryKit() {
        Map<String, List<DicType>> model = Maps.newHashMap();
        List<DicType> dicTypes = Lists.newArrayList();
        model.put("dicTypes", dicTypes);
        String[] array1 = StrKit.split(params, "|");
        for (String str : array1) {
            String[] tempArr = StrKit.split(str, ":");
            String[] valArr = StrKit.split(tempArr[1], ",");

            DicType dicType = new DicType();
            dicType.setName(tempArr[0]);
            dicType.setCode(PinYinKit.getPinYinHeadChar(tempArr[0]).toUpperCase());
            dicTypes.add(dicType);


            List<Dic> dics = Lists.newArrayList();
            for (String val : valArr) {
                String[] arr = StrKit.split(val, "=");
                Dic dic = new Dic();
                Map<String, String> params = Maps.newHashMap();
                params.put("\\(", "_");
                params.put("\\)", "");

                dic.setName(arr[1]);
                dic.setCode(StrKit.replace(PinYinKit.getPinYinHeadChar(arr[1]).toUpperCase(), params));
                dic.setOrigCode(PinYinKit.getPinYinHeadChar(arr[1]).toUpperCase());
                dics.add(dic);
            }
            dicType.setDics(dics);
        }

        TemplateKit.executeFreemarker("E:/work/svn/shark/trunk/shark-code-gen/src/main/resources", "DictionaryKit.ftl", CharsetKit.DEFAULT_ENCODE, model, "E:/work/svn/shark/trunk/shark-service/src/main/java/com/credithc/shark/dictionary", "DictionaryKit.java");
    }

    private static void genLogKit() {
        TemplateKit.executeFreemarker("E:/work/svn/shark/trunk/shark-code-gen/src/main/resources", "LogKit.ftl", CharsetKit.DEFAULT_ENCODE, null, "E:/work/svn/shark/trunk/shark-service/src/main/java/com/credithc/shark/dictionary", "LogKit.java");
    }

    private static void initDictionary() {
        String[] array1 = StrKit.split(params, "|");

        int i = 1;
        for (String str : array1) {
            String[] tempArr = StrKit.split(str, ":");
            String[] valArr = StrKit.split(tempArr[1], ",");

            for (String val : valArr) {
                String[] arr = StrKit.split(val, "=");

                System.out.println(
                    StrKit.format("INSERT INTO `bd_dictionary` " +
                            "(`id`, `name`, `code`, `value`, `type`, `sort`, `pid`, `pcode`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                            "VALUES " +
                            "({}, '{}', '{}', {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {});",
                        i++, arr[1], PinYinKit.getPinYinHeadChar(arr[1]).toUpperCase(), Integer.valueOf(arr[0].trim()), PinYinKit.getPinYinHeadChar(tempArr[0]).toUpperCase(), 0, 0, "", System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));
            }
        }
    }

    private static String resources = "配置管理=" +
        "{审批配置[服务部配置:查询-配置,审批链配置:查询-配置]};" +
        "{业务配置[拒绝原因配置:新增-删除,回退原因配置:新增-删除,黑名单配置:新增-删除]};" +
        "{审批金额配置[确定-取消]};" +
        "{授权配置[本人授权:查询-确定-取消,三方授权:查询-确定-取消]}|" +
        "用户管理=" +
        "{人员管理[查询-添加-修改-删除-密码重置-刷新-配置]};" +
        "{角色管理[查询-添加-修改-删除-刷新-配置]};" +
        "{机构管理[添加-修改-删除]};" +
        "{菜单权限[添加-修改-删除]}|" +
        "定价管理=" +
        "{待分配列表[查询-分配-全量分配]};" +
        "{定价汇总表[查询-综合查询-查看]};" +
        "{待定价列表[查询-定价-查看]};" +
        "{已定价列表[查询-查看]};" +
        "{定价页[基本信息-承租人信息-租赁物信息-配置检测-技术及工况检测-担保信息-评估信息-三方数据-车鉴定-定价-四项核查-信息审核-信审结论-轨迹-挂起-提交决策]}|" +
        "信审管理=" +
        "{信审汇总表[查询-综合查询-查看]};" +
        "{待审核列表[查询-审核-查看]};" +
        "{已审核列表[查询-查看]};" +
        "{审核页[基本信息-承租人信息-租赁物信息-配置检测-技术及工况检测-担保信息-评估信息-三方数据-车鉴定-定价-四项核查-信息审核-信审结论-轨迹-挂起-提交决策]};" +
        "{黑名单列表[查询-导出-导入-查看-删除]}|" +
        "数据统计=" +
        "{审核信息表[查询-导出]}";

    private static void initResourcesAndRole() {
        String[] array2 = StrKit.split(resources, "|");

        int j = 1;
        int m = 1;
        for (String str : array2) {
            String[] arr = str.split("=");
            String menu1 = arr[0];
            int pid = j;

            System.out.println(
                StrKit.format("INSERT INTO `su_resource` " +
                        "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                        "VALUES " +
                        "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                    j++, menu1, PinYinKit.getPinYinHeadChar(menu1).toUpperCase(), "", "", "", 0, 0, 1, 0, 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

            String[] arr2 = arr[1].split(";");
            for (String str1 : arr2) {

                String[] arr3 = str1.split("\\[");
                String subMenu = arr3[0].substring(1);
                int pid1 = j;

                System.out.println(
                    StrKit.format("INSERT INTO `su_resource` " +
                            "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                            "VALUES " +
                            "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                        j++, subMenu, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + "_" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase(), "", "", "", pid, 0, 1, 1, 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                        "VALUES " +
                        "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                    m++, 1, "超级管理员", "CJGLY", pid1, subMenu, PinYinKit.getPinYinHeadChar(menu1).toUpperCase() + "_" + PinYinKit.getPinYinHeadChar(subMenu).toUpperCase(), 1, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                String[] arr4 = arr3[1].substring(0, arr3[1].lastIndexOf("]")).split(",");
                for (String str2 : arr4) {

                    String[] arr6;
                    if (str2.contains(":")) {
                        String[] arr5 = str2.split(":");
                        String op1 = arr5[0];
                        System.out.println(
                            StrKit.format("INSERT INTO `su_resource` " +
                                    "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                    "VALUES " +
                                    "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                j, op1, PinYinKit.getPinYinHeadChar(op1).toUpperCase(), "", "", "", pid1, 0, 0, 0, 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                        int pid2 = j++;
                        arr6 = StrKit.split(arr5[1], "-");

                        for (String str3 : arr6) {
                            int tempId = j;
                            System.out.println(
                                StrKit.format("INSERT INTO `su_resource` " +
                                        "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                        "VALUES " +
                                        "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                    j++, str3, PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + "_" + PinYinKit.getPinYinHeadChar(op1).toUpperCase() + "_" + PinYinKit.getPinYinHeadChar(str3).toUpperCase(), "", "", "", pid2, 0, 0, 0, 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                            System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                    "VALUES " +
                                    "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                m++, 1, "超级管理员", "CJGLY", tempId, str3, PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + "_" + PinYinKit.getPinYinHeadChar(op1).toUpperCase() + "_" + PinYinKit.getPinYinHeadChar(str3).toUpperCase(), 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));
                        }
                    } else {
                        arr6 = StrKit.split(str2, "-");

                        for (String str3 : arr6) {
                            int tempId = j;
                            System.out.println(
                                StrKit.format("INSERT INTO `su_resource` " +
                                        "(`id`, `name`, `code`, `url`, `icon`, `remark`, `pid`, `sort`, `is_menu`, `is_menu_leaf`, `is_data_security`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                        "VALUES " +
                                        "({}, '{}', '{}', '{}', '{}', '{}', {}, {}, {}, {}, {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                    j++, str3, PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + "_" + PinYinKit.getPinYinHeadChar(str3).toUpperCase(), "", "", "", pid1, 0, 0, 0, 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));

                            System.out.println(StrKit.format("INSERT INTO `su_role_resource` (`id`, `role_id`, `role_name`, `role_code`, `resource_id`, `resource_name`, `resource_code`, `is_menu`, `create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) " +
                                    "VALUES " +
                                    "({}, {}, '{}', '{}', {}, '{}', '{}', {}, {}, {}, '{}', {}, {}, '{}', {}, {});",
                                m++, 1, "超级管理员", "CJGLY", tempId, str3, PinYinKit.getPinYinHeadChar(subMenu).toUpperCase() + "_" + PinYinKit.getPinYinHeadChar(str3).toUpperCase(), 0, System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0));
                        }
                    }
                }
            }
        }
    }

    private static void initPriceCarInfo() {
        String priceCarInfo = "price_code1;123;customer_name01;123;220203198510131254;18944141414;lessee_name01;21;123;assess_name01;123;1;pricing_name01;123;1;credit_name01;123;1;credit_master_name01;123;" +
            "1;credit_manager_name01;123;1;credit_director_name01;123;1;credit_ceo_name;" +
            "123;1;credit_last_name01;1;credit_last_role_name01;big_depart_name01;city_name01;1;service_depart_name;" +
            "service_person_name;12;1;0;0;123;channel_tyoe01;channel_name01;123;product_type01;product_type_name01-";

        String[] arr = priceCarInfo.split("-");

        String insertTemplate = "INSERT INTO `pt_pricing_task` \n" +
            "(`id`, `input_piece_id`, `input_piece_code`, `input_piece_at`, `customer_name`, `customer_code`, `id_card_no`, `mobile`, `lessee_name`, `assess_amount`, `assess_at`, `assess_name`, \n" +
            "`pricing_at`, `pricing_id`, `pricing_name`, `credit_at`, `credit_id`, `credit_name`, `credit_master_at`, `credit_master_id`, `credit_master_name`, `credit_manager_at`,\n" +
            "`credit_manager_id`, `credit_manager_name`, `credit_director_at`, `credit_director_id`, `credit_director_name`, `credit_ceo_at`, `credit_ceo_id`, `credit_ceo_name`, \n" +
            "`credit_last_at`, `credit_last_id`, `credit_last_name`, `credit_last_role_id`, `credit_last_role_name`, `big_depart_name`, `city_name`, `service_depart_id`, `service_depart_name`, \n" +
            "`service_person_name`, `status_val`, `status`, `status_val_before_hang`, `status_before_hang`, `channel_val`, `channel_type`, `channel_name`, `product_val`, `product_type`, `product_type_name`, \n" +
            "`create_at`, `create_by`, `create_name`, `update_at`, `update_by`, `update_name`, `is_del`, `is_test`) \n" +
            "VALUES \n" +
            "({}, {}, '{}', {}, '{}', '{}', '{}', '{}', '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, {}, '{}', {}, '{}', '{}', '{}', {}, '{}', '{}', \n" +
            "{}, '{}', {}, '{}', {}, '{}', '{}', {}, '{}', '{}',\n" +
            "{}, {}, '{}', {}, {}, '{}', {}, {});\n";
        for (String info : arr) {
            int i = 1;
            int j = 100;
            System.out.println("info is " + info);
            String[] k = info.split(";");
            System.out.println("k size is " + k.length);
            String sql = StrKit.format(insertTemplate, i++, j++,
                k[0], k[1], k[2], k[3], k[4], k[5], k[6], k[7], k[8], k[9], k[10], k[11], k[12], k[13],
                k[14], k[15], k[16], k[17], k[18], k[19], k[20],
                k[21], k[22], k[23], k[24], k[25], k[26], k[27], k[28], k[29],
                k[30], k[31], k[32], k[33], k[34], k[35], k[36], k[37], k[38],
                k[39], k[40], k[41], k[42], k[43], k[44], k[45], k[46], k[47],
                System.currentTimeMillis(), 0, "SYSTEM", System.currentTimeMillis(), 0, "SYSTEM", 0, 0);

            System.out.println(sql);
        }
    }
}
