<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <parent>
        <artifactId>${conf.parentArtifactId}</artifactId>
        <groupId>${conf.parentGroupId}</groupId>
        <version>3.0-SNAPSHOT</version>
    </parent>
    <modelVersion>4.0.0</modelVersion>

    <artifactId>${conf.webModuleName}</artifactId>
    <packaging>war</packaging>

    <dependencies>
        <!-- Module -->
        <dependency>
            <groupId>${conf.parentGroupId}</groupId>
            <artifactId>${conf.shiroModuleName}</artifactId>
            <version>3.0-SNAPSHOT</version>
        </dependency>
        <!-- END -->

        <!-- web依赖包 -->
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>servlet-api</artifactId>
        </dependency>
        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>jsp-api</artifactId>
        </dependency>
        <!-- END -->

        <!-- jetty begin -->
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-webapp</artifactId>
        </dependency>
        <dependency>
            <groupId>org.eclipse.jetty</groupId>
            <artifactId>jetty-jsp</artifactId>
        </dependency>
        <!-- jetty end -->

        <!-- test -->
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
        </dependency>
        <!-- END -->
    </dependencies>

    <build>
        <finalName>${r'${'}project.artifactId}</finalName>
        <resources>
            <resource>
                <directory>src/main/resources/</directory>
                <includes>
                    <include>*.xml</include>
                </includes>
            </resource>
            <resource>
                <directory>src/main/resources/${r'${'}resourcePath}/</directory>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.apache.tomcat.maven</groupId>
                <artifactId>tomcat6-maven-plugin</artifactId>
                <configuration>
                    <port>8080</port>
                    <path>/${r'${'}project.artifactId}</path>
                    <uriEncoding>${r'${'}project.encoding}</uriEncoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.tomcat.maven</groupId>
                <artifactId>tomcat7-maven-plugin</artifactId>
                <configuration>
                    <port>8080</port>
                    <path>/${r'${'}project.artifactId}</path>
                    <uriEncoding>${r'${'}project.encoding}</uriEncoding>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.mortbay.jetty</groupId>
                <artifactId>jetty-maven-plugin</artifactId>
            </plugin>
        </plugins>
    </build>
</project>
