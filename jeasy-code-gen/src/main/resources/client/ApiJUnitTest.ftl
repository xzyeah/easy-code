package ${conf.basePackage}.${table.lowerCamelName}.api;

import ${conf.basePackage}.BaseJUnitTester4SpringContext;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * ${table.comment} ApiJUnitTest
 *
 * @author ${conf.author}
 * @version ${conf.version}
 * @since ${conf.createDate}
 */
@Slf4j
public class ${table.className}ApiJUnitTest extends BaseJUnitTester4SpringContext {

    @Autowired
    private ${table.className}Api ${table.camelName}Api;

    @Test
    public void testList() {
        System.out.println(${table.camelName}Api.list(null).toString());
    }

    @Test
    public void testList1_1_0() {
        System.out.println(${table.camelName}Api.list1_1_0(null).toString());
    }

    @Test
    public void testList1_2_0() {
        System.out.println(${table.camelName}Api.list1_2_0(null).toString());
    }

    @Test
    public void testList1_3_0() {
        System.out.println(${table.camelName}Api.list1_3_0(null).toString());
    }

    @Test
    public void testPage() {
        System.out.println(${table.camelName}Api.page(null, 1, 10).toString());
    }

    @Test
    public void testAdd() {
        System.out.println(${table.camelName}Api.add(null).toString());
    }

    @Test
    public void testShow() {
        System.out.println(${table.camelName}Api.show(2l).toString());
    }

    @Test
    public void testModify() {
        System.out.println(${table.camelName}Api.modify(null).toString());
    }

    @Test
    public void testRemove() {
        System.out.println(${table.camelName}Api.remove(2l).toString());
    }
}
