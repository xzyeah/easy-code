package com.jeasy.organization.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 机构
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
@Data
@TableName("su_organization")
public class OrganizationEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 编码
     */
    @TableField("code")
    private String code;

    /**
     * 地址
     */
    @TableField("address")
    private String address;

    /**
     * 机构类型值
     */
    @TableField("type_val")
    private Integer typeVal;

    /**
     * 机构类型编码:字典
     */
    @TableField("type_code")
    private String typeCode;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 是否叶子节点:0=否,1=是
     */
    @TableField("is_leaf")
    private Integer isLeaf;

    /**
     * 父ID
     */
    @TableField("pid")
    private Long pid;

    /**
     * 图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 备注/描述
     */
    @TableField("remark")
    private String remark;

}
