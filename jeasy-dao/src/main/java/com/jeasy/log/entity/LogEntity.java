package com.jeasy.log.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 日志
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
@Data
@TableName("bd_log")
public class LogEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 表名称
     */
    @TableField("table_name")
    private String tableName;

    /**
     * 记录ID
     */
    @TableField("record_id")
    private Long recordId;

    /**
     * 字段名称
     */
    @TableField("field_name")
    private String fieldName;

    /**
     * 日志类型值:3020=定价日志,3021=信审日志,3022=普通日志
     */
    @TableField("log_type_val")
    private Integer logTypeVal;

    /**
     * 日志类型编码:字典
     */
    @TableField("log_type_code")
    private String logTypeCode;

    /**
     * 操作类型值:3030=创建,3031=删除,3032=修改,3033=拒绝,3034=通过,3035=回退
     */
    @TableField("opt_type_val")
    private Integer optTypeVal;

    /**
     * 操作类型编码:字典
     */
    @TableField("opt_type_code")
    private String optTypeCode;

    /**
     * 操作类型描述
     */
    @TableField("opt_desc")
    private String optDesc;

    /**
     * 操作前值
     */
    @TableField("before_value")
    private String beforeValue;

    /**
     * 操作后值
     */
    @TableField("after_value")
    private String afterValue;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

}
