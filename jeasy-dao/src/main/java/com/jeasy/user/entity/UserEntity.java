package com.jeasy.user.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 用户
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
@Data
@TableName("su_user")
public class UserEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 登陆名
     */
    @TableField("login_name")
    private String loginName;

    /**
     * 编码
     */
    @TableField("code")
    private String code;

    /**
     * 密码
     */
    @TableField("pwd")
    private String pwd;

    /**
     * 加密盐
     */
    @TableField("salt")
    private String salt;

    /**
     * 手机号
     */
    @TableField("mobile")
    private String mobile;

    /**
     * 身份证号
     */
    @TableField("id_card_no")
    private String idCardNo;

    /**
     * 用户状态值:1000=启用,1001=停用
     */
    @TableField("status_val")
    private Integer statusVal;

    /**
     * 用户状态编码:字典
     */
    @TableField("status_code")
    private String statusCode;

    /**
     * 权限类型值:1010=当前用户,1011=所属机构,1012=所有数据
     */
    @TableField("data_security_val")
    private Integer dataSecurityVal;

    /**
     * 权限类型编码:字典
     */
    @TableField("data_security_code")
    private String dataSecurityCode;

    /**
     * 备注
     */
    @TableField("remark")
    private String remark;

}
