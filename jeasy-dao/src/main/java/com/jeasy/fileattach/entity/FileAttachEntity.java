package com.jeasy.fileattach.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 文件附件
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
@Data
@TableName("bd_file_attach")
public class FileAttachEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 表名称
     */
    @TableField("table_name")
    private String tableName;

    /**
     * 记录ID
     */
    @TableField("record_id")
    private Long recordId;

    /**
     * 文件原名称
     */
    @TableField("name")
    private String name;

    /**
     * 文件URL
     */
    @TableField("url")
    private String url;

    /**
     * 文件图标URL
     */
    @TableField("icon_url")
    private String iconUrl;

    /**
     * 文件预览URL
     */
    @TableField("preview_url")
    private String previewUrl;

}
