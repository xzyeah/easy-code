package com.jeasy.userorg.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.userorg.entity.UserOrgEntity;

/**
 * 用户机构 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
public interface UserOrgDAO extends BaseDAO<UserOrgEntity> {
}
