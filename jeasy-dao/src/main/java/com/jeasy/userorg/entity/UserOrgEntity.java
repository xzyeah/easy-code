package com.jeasy.userorg.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 用户机构
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
@Data
@TableName("su_user_org")
public class UserOrgEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 用户ID
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 用户名称
     */
    @TableField("user_name")
    private String userName;

    /**
     * 用户标示
     */
    @TableField("user_code")
    private String userCode;

    /**
     * 机构ID
     */
    @TableField("org_id")
    private Long orgId;

    /**
     * 机构名称
     */
    @TableField("org_name")
    private String orgName;

    /**
     * 机构编码
     */
    @TableField("org_code")
    private String orgCode;

}
