package com.jeasy.dictionary.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.dictionary.entity.DictionaryEntity;

/**
 * 字典 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
public interface DictionaryDAO extends BaseDAO<DictionaryEntity> {
}
