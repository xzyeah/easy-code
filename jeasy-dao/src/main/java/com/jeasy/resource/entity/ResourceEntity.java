package com.jeasy.resource.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 菜单
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
@Data
@TableName("su_resource")
public class ResourceEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 名称
     */
    @TableField("name")
    private String name;

    /**
     * 编码
     */
    @TableField("code")
    private String code;

    /**
     * URL
     */
    @TableField("url")
    private String url;

    /**
     * 图标
     */
    @TableField("icon")
    private String icon;

    /**
     * 备注/描述
     */
    @TableField("remark")
    private String remark;

    /**
     * 父ID
     */
    @TableField("pid")
    private Long pid;

    /**
     * 排序
     */
    @TableField("sort")
    private Integer sort;

    /**
     * 是否菜单:0=否,1=是
     */
    @TableField("is_menu")
    private Integer isMenu;

    /**
     * 是否叶子节点:0=否,1=是
     */
    @TableField("is_menu_leaf")
    private Integer isMenuLeaf;

    /**
     * 是否数据权限:0=否,1=是
     */
    @TableField("is_data_security")
    private Integer isDataSecurity;

}
