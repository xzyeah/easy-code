package com.jeasy.roleresource.dao;

import com.jeasy.base.mybatis.dao.BaseDAO;
import com.jeasy.roleresource.entity.RoleResourceEntity;

/**
 * 角色资源 DAO
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
public interface RoleResourceDAO extends BaseDAO<RoleResourceEntity> {
}
