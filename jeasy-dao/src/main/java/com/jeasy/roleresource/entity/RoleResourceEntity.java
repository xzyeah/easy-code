package com.jeasy.roleresource.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.jeasy.base.mybatis.entity.BaseEntity;
import lombok.Data;

/**
 * 角色资源
 *
 * @author taomk
 * @version 1.0
 * @since 2017/05/31 21:06
 */
@Data
@TableName("su_role_resource")
public class RoleResourceEntity extends BaseEntity {

    private static final long serialVersionUID = 5409185459234711691L;

    /**
     * 角色ID
     */
    @TableField("role_id")
    private Long roleId;

    /**
     * 角色名称
     */
    @TableField("role_name")
    private String roleName;

    /**
     * 角色编码
     */
    @TableField("role_code")
    private String roleCode;

    /**
     * 资源ID
     */
    @TableField("resource_id")
    private Long resourceId;

    /**
     * 资源名称
     */
    @TableField("resource_name")
    private String resourceName;

    /**
     * 资源编码
     */
    @TableField("resource_code")
    private String resourceCode;

    /**
     * 是否菜单:0=否,1=是
     */
    @TableField("is_menu")
    private Integer isMenu;

}
